/*
 * LagrangePolynomialWidget.cpp
 *
 *  Created on: 04-02-2013
 *      Author: Revers
 */

#include "LagrangePolynomialWidget.h"

#include <iostream>
#include <QApplication>
#include <QPainter>
#include <QButtonGroup>
#include <QMouseEvent>
#include <QRect>
//#include <rev/common/RevAssert.h>

#include "ui_QtCurvesAndSurfacesWindow.h"

using namespace rev;
using namespace std;

LagrangePolynomialWidget::LagrangePolynomialWidget(Ui::MainWindow* mainWindow,
        QWidget* parent) :
        bgColor(203, 214, 230) {
    this->mainWindow = mainWindow;

    QButtonGroup* group = new QButtonGroup(this);
    group->addButton(mainWindow->lagrangePolyLagrangeMethodCB);
    group->addButton(mainWindow->lagrangePolyNewtonMethodCB);

    setupActions();
}

LagrangePolynomialWidget::~LagrangePolynomialWidget() {
}

void LagrangePolynomialWidget::setupActions() {
    connect(mainWindow->lagrangePolyClearB, SIGNAL(clicked()),
            this, SLOT(clearAction()));

    connect(mainWindow->lagrangePolyLagrangeMethodCB, SIGNAL(toggled(bool)),
            this, SLOT(lagrangeMethodChecked(bool)));

    connect(mainWindow->lagrangePolyNewtonMethodCB, SIGNAL(toggled(bool)),
            this, SLOT(newtonMethodChecked(bool)));
}

void LagrangePolynomialWidget::clear() {
    points.clear();
    focusPointIndex = -1;
    mainWindow->lagrangePolyPointsL->setText(QString("Points: 0"));
    repaint();
}

void LagrangePolynomialWidget::mousePressEvent(QMouseEvent* event) {
    if (event->button() == Qt::LeftButton) {
        QPoint p = event->pos();
        mousePressed = true;

        if (!hitPoint(p)) {
            focusPointIndex = points.size();
            points.push_back(vec2_t(p.x(), p.y()));
            mainWindow->lagrangePolyPointsL->setText(
                    QString("Points: %1").arg(points.size()));
        }
    }

    repaint();
}

void LagrangePolynomialWidget::mouseReleaseEvent(QMouseEvent* event) {
    mousePressed = false;
    focusPointIndex = -1;
    repaint();
}

void LagrangePolynomialWidget::mouseMoveEvent(QMouseEvent* event) {
    if (!mousePressed) {
        return;
    }

    moveFocusedPoint(event->pos());
    repaint();
}

bool LagrangePolynomialWidget::hitPoint(const QPoint& p) {
    int index = 0;
    for (vec2_t& v : points) {
        if (glm::distance(vec2_t(p.x(), p.y()), v) <= POINT_RADIUS + 2) {
            focusPointIndex = index;
            return true;
        }
        index++;
    }
    focusPointIndex = -1;
    return false;
}

void LagrangePolynomialWidget::moveFocusedPoint(const QPoint& p) {
    if (focusPointIndex < 0) {
        return;
    }

    points[focusPointIndex] = vec2_t(p.x(), p.y());
}

scalar_t LagrangePolynomialWidget::newtonMethod(scalar_t x) {
    return 50;
}

scalar_t LagrangePolynomialWidget::lagrangePoly(scalar_t t, int degree, int index) {
    if (degree < 1) {
        return 0;
    }

    scalar_t step = scalar_t(1) / scalar_t(degree - 1);
    scalar_t val(1);
    scalar_t t_i = step * index;

    for (int j = 0; j < degree; j++) {
        if (index == j) {
            continue;
        }
        scalar_t t_j = step * j;

        val *= (t - t_j) / (t_i - t_j);
    }

    return val;
}

vec2_t LagrangePolynomialWidget::baryLagrangeMethod(scalar_t t) {
    vec2_t val(0);

    for (int i = 0; i < points.size(); i++) {
        val += points[i] * lagrangePoly(t, points.size(), i);
    }

    return val;
}

scalar_t LagrangePolynomialWidget::lagrangeMethod(scalar_t x) {
    if (points.size() < 1) {
        return 0;
    }

    scalar_t val(0);
    for (int i = 0; i < points.size(); i++) {
        const vec2_t& p_i = points[i];
        const scalar_t& x_i = p_i.x;
        const scalar_t& y_i = p_i.y;

        scalar_t lp_i(1);
        for (int j = 0; j < points.size(); j++) {
            if (i == j) {
                continue;
            }
            const vec2_t& p_j = points[j];
            const scalar_t& x_j = p_j.x;
            const scalar_t& y_j = p_j.y;

            lp_i *= (x - x_j) / (x_i - x_j);
        }

        val += y_i * lp_i;
    }

    return val;
}

void LagrangePolynomialWidget::drawLagrangeSimple(QPainter& painter, int width) {
    int lastX = 0;
    int lastY = lagrangeMethod(0);
    for (int x = 0; x < width; x++) {
        int y = lagrangeMethod(x);

        painter.drawLine(x, y, lastX, lastY);
        lastX = x;
        lastY = y;
    }
}

void LagrangePolynomialWidget::drawBarycentricLagrange(QPainter& painter) {
    for (scalar_t t = 0; t < 1; t += 0.01) {
        vec2_t v1 = baryLagrangeMethod(t);
        vec2_t v2 = baryLagrangeMethod(t + 0.01);

        painter.drawLine((int) v1.x, (int) v1.y,
                (int) v2.x, (int) v2.y);
    }
}

scalar_t LagrangePolynomialWidget::newtonPoly(scalar_t t, int degree,
        int index) {
    if (degree < 1) {
        return 1;
    }

    scalar_t step = scalar_t(1) / scalar_t(degree - 1);

    scalar_t val(1);
    for (int i = 0; i < index; i++) {
        scalar_t t_i = step * i;
        val *= (t - t_i);
    }

    return val;
}

#define ARR(x,y) arr[(x) * n + (y)]

vec2_t LagrangePolynomialWidget::baryNewtonMethod(scalar_t t, vec2_t* arr) {
    vec2_t val(0);
    int n = points.size();

    for (int i = 0; i < n; i++) {
        val += newtonPoly(t, n, i) * ARR(i, 0);
    }

    return val;
}

void LagrangePolynomialWidget::drawBarycentricNewton(QPainter& painter) {
    int n = points.size();
    scalar_t step = scalar_t(1) / scalar_t(n - 1);

    //-------------------------------------------------------------------------------
    vec2_t* arr = new vec2_t[n * n];
    for (int y = 0; y < n; y++) {
        ARR(0, y)= points[y];
    }

    for (int x = 1; x < n; x++) {
        for (int y = 0; y < n - x; y++) {
            scalar_t t_y = step * y;
            scalar_t t_k = step * (x + y);
            ARR(x, y)= (ARR(x - 1, y + 1) - ARR(x - 1, y)) / (t_k - t_y);
        }
    }
    //===============================================================================

    for (scalar_t t = 0; t < 1; t += 0.01) {
        vec2_t v1 = baryNewtonMethod(t, arr);
        vec2_t v2 = baryNewtonMethod(t + 0.01, arr);

        painter.drawLine((int) v1.x, (int) v1.y,
                (int) v2.x, (int) v2.y);
    }

    delete[] arr;
}

void LagrangePolynomialWidget::paintEvent(QPaintEvent* event) {
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    painter.setPen(bgColor);
    painter.setBrush(QBrush(bgColor));
    int w = width() - 1;
    painter.drawRect(0, 0, w, height() - 1);

    painter.setPen(Qt::blue);
    drawLagrangeSimple(painter, w);

    if (useLagrange) {
        painter.setPen(Qt::red);
        drawBarycentricLagrange(painter);
    } else {
        painter.setPen(Qt::green);
        drawBarycentricNewton(painter);
    }

    if (showControlPoints) {
        drawControlPoints(&painter);
    }
}

void LagrangePolynomialWidget::drawControlPoints(QPainter* p) {
    QPainter& painter = *p;

    for (int i = 0; i < points.size(); i++) {
        if (i == focusPointIndex) {
            painter.setBrush(QBrush(Qt::red));
            painter.setPen(Qt::red);
        } else {
            painter.setBrush(QBrush(Qt::black));
            painter.setPen(Qt::black);
        }

        const vec2_t& v = points[i];
        painter.drawEllipse((int) v.x - POINT_RADIUS,
                (int) v.y - POINT_RADIUS, POINT_RADIUS * 2 + 1,
                POINT_RADIUS * 2);
    }
}

void LagrangePolynomialWidget::clearAction() {
    clear();
}

void LagrangePolynomialWidget::lagrangeMethodChecked(bool checked) {
    useLagrange = checked;
    repaint();
}

void LagrangePolynomialWidget::newtonMethodChecked(bool checked) {
    useLagrange = !checked;
    repaint();
}
