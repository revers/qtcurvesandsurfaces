/*
 * LagrangePolynomialWidget.h
 *
 *  Created on: 04-02-2013
 *      Author: Revers
 */

#ifndef LAGRANGEPOLYNOMIALWIDGET_H_
#define LAGRANGEPOLYNOMIALWIDGET_H_

#include <iostream>
#include <vector>

#include <QWidget>
#include <QSize>
#include <QColor>

#include "QtCurvesAndSurfacesConfig.h"

class QMouseEvent;
class QPainter;

namespace Ui {
    class MainWindow;
}

namespace rev {

    class LagrangePolynomialWidget: public QWidget {
    Q_OBJECT

        static const int POINT_RADIUS = 4;

        std::vector<vec2_t> points;

        QColor bgColor;
        bool showControlLines = true;
        bool showControlPoints = true;
        bool mousePressed = false;
        bool useLagrange = true;

        int focusPointIndex = -1;

        Ui::MainWindow* mainWindow;

    public:
        LagrangePolynomialWidget(Ui::MainWindow* mainWindow,
                QWidget* parent = 0);

        virtual ~LagrangePolynomialWidget();

        void clear();

        bool isShowControlLines() const {
            return showControlLines;
        }

        bool isShowControlPoints() const {
            return showControlPoints;
        }

    protected:
        void paintEvent(QPaintEvent* event) override;
        void mousePressEvent(QMouseEvent* event) override;
        void mouseReleaseEvent(QMouseEvent* event) override;
        void mouseMoveEvent(QMouseEvent* event) override;

    private:
        scalar_t lagrangePoly(scalar_t t, int degree, int index);
        vec2_t baryLagrangeMethod(scalar_t t);
        scalar_t newtonPoly(scalar_t t, int degree, int index);
        vec2_t baryNewtonMethod(scalar_t t, vec2_t* arr);
        void drawLagrangeSimple(QPainter& painter, int width);
        void drawBarycentricLagrange(QPainter& painter);
        void drawBarycentricNewton(QPainter& painter);

        scalar_t lagrangeMethod(scalar_t x);
        scalar_t newtonMethod(scalar_t x);
        bool hitPoint(const QPoint& p);
        void moveFocusedPoint(const QPoint& p);

        void setupActions();
        void drawControlPoints(QPainter* p);

        virtual QSize sizeHint() const override {
            return QSize(300, 100);
        }

    private slots:
        void clearAction();
        void lagrangeMethodChecked(bool checked);
        void newtonMethodChecked(bool checked);
    };

} /* namespace rev */
#endif /* LAGRANGEPOLYNOMIALWIDGET_H_ */
