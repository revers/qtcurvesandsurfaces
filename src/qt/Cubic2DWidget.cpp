/*
 * Cubic2DWidget.cpp
 *
 *  Created on: 22-11-2012
 *      Author: Revers
 */

#include "Cubic2DWidget.h"

#include <iostream>
#include <QApplication>
#include <QPainter>
#include <QMouseEvent>
#include <QRect>
#include "../RevBaseMatrix.h"

#include "ui_QtCurvesAndSurfacesWindow.h"

using namespace rev;
using namespace std;

Cubic2DWidget::Cubic2DWidget(Ui::MainWindow* mainWindow,
        QWidget* parent) :
        bgColor(203, 214, 230) {
    this->mainWindow = mainWindow;

    cubicType = CubicType::BEZIER;
    baseMatrix = rev::BaseMatrix<scalar_t>::BEZIER;

    this->mainWindow->cubic2DCurveTypeCB->addItem("Bezier");
    this->mainWindow->cubic2DCurveTypeCB->addItem("Hermite");
    this->mainWindow->cubic2DCurveTypeCB->addItem("Bspline");
    this->mainWindow->cubic2DCurveTypeCB->addItem("Catmull-Rom");
    setupActions();
}

Cubic2DWidget::~Cubic2DWidget() {
}

void Cubic2DWidget::setupActions() {
    connect(mainWindow->cubic2DClearPB, SIGNAL(clicked()),
            this, SLOT(clearAction()));

    connect(mainWindow->cubic2DCurveTypeCB, SIGNAL(currentIndexChanged(int)),
            this, SLOT(curveTypeChangedAction(int)));

    connect(mainWindow->cubic2DShowControlLinesCB, SIGNAL(toggled(bool)),
            this, SLOT(setShowControlLines(bool)));

    connect(mainWindow->cubic2DShowControlPointsCB, SIGNAL(toggled(bool)),
            this, SLOT(setShowControlPoints(bool)));

    connect(mainWindow->cubic2DdrawNormalsCB, SIGNAL(toggled(bool)),
            this, SLOT(drawNormalsSelected(bool)));

    connect(mainWindow->cubic2DnormalSlider, SIGNAL(valueChanged(int)),
            this, SLOT(drawNormalsSliderValueChanged(int)));
}

void Cubic2DWidget::mousePressEvent(QMouseEvent* event) {
    if (event->button() == Qt::LeftButton) {
        QPoint p = event->pos();
        mousePressed = true;

        if (!hitPoint(p)) {
            focusPointIndex = points.size();
            points.push_back(vec2_t(p.x(), p.y()));
        }
    }

    repaint();
}

void Cubic2DWidget::mouseReleaseEvent(QMouseEvent* event) {
    mousePressed = false;
    focusPointIndex = -1;
    repaint();
}

void Cubic2DWidget::mouseMoveEvent(QMouseEvent* event) {
    if (!mousePressed) {
        return;
    }

    moveFocusedPoint(event->pos());
    repaint();
}

bool Cubic2DWidget::hitPoint(const QPoint& p) {
    int index = 0;
    for (vec2_t& v : points) {
        if (glm::distance(vec2_t(p.x(), p.y()), v) <= POINT_RADIUS + 2) {
            focusPointIndex = index;
            return true;
        }
        index++;
    }
    focusPointIndex = -1;
    return false;
}

void Cubic2DWidget::moveFocusedPoint(const QPoint& p) {
    if (focusPointIndex < 0) {
        return;
    }

    points[focusPointIndex] = vec2_t(p.x(), p.y());
}

void Cubic2DWidget::paintEvent(QPaintEvent* event) {
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    painter.setPen(bgColor);
    painter.setBrush(QBrush(bgColor));
    painter.drawRect(0, 0, width() - 1, height() - 1);

    if (showControlLines) {
        drawControlLines(&painter);
    }

    for (int i = 0; i < points.size()
            && points.size() - i >= 4; i += 3) {

        Cubic2D curve(baseMatrix,
                points[i], points[i + 1],
                points[i + 2], points[i + 3]);

        painter.setPen(Qt::red);

        for (scalar_t t = 0; t < 1; t += 0.01) {
            vec2_t v1 = curve.eval(t);
            vec2_t v2 = curve.eval(t + 0.01);

            painter.drawLine((int) v1.x, (int) v1.y,
                    (int) v2.x, (int) v2.y);
        }

        if (drawNormals) {
            drawNormal(&painter, curve, normalPosition);
        }
    }

    if (showControlPoints) {
        drawControlPoints(&painter);
    }
}

void Cubic2DWidget::drawControlLines(QPainter* p) {
    QPainter& painter = *p;

    painter.setPen(Qt::gray);
    for (int i = 1; i < points.size(); i++) {
        const vec2_t& v1 = points[i];
        const vec2_t& v2 = points[i - 1];

        painter.drawLine((int) v1.x, (int) v1.y,
                (int) v2.x, (int) v2.y);
    }
}

void Cubic2DWidget::drawControlPoints(QPainter* p) {
    QPainter& painter = *p;

    for (int i = 0; i < points.size(); i++) {
        if (i == focusPointIndex) {
            painter.setBrush(QBrush(Qt::red));
            painter.setPen(Qt::red);
        } else if (i % 3 == 0) {
            painter.setBrush(QBrush(Qt::blue));
            painter.setPen(Qt::blue);
        } else {
            painter.setBrush(QBrush(Qt::black));
            painter.setPen(Qt::black);
        }

        const vec2_t& v = points[i];
        painter.drawEllipse((int) v.x - POINT_RADIUS,
                (int) v.y - POINT_RADIUS, POINT_RADIUS * 2 + 1,
                POINT_RADIUS * 2);
    }
}

void Cubic2DWidget::drawNormal(QPainter* p, Cubic2D& curve, scalar_t pos) {
    QPainter& painter = *p;
    painter.setPen(Qt::green);
    vec2_t v = curve.eval(pos);
    vec2_t dv = curve.getFirstDerivative(pos);
    vec2_t ddv = curve.getSecondDerivative(pos);
    scalar_t dvLen = glm::length(dv);

    vec2_t k = ddv - (glm::dot(ddv, dv) / (dvLen * dvLen)) * dv;

    // tangent vector
    vec2_t t = glm::normalize(dv);

    // principal normal vector
    vec2_t n = glm::normalize(k);

    t *= 40.0;
    n *= 40.0;
    painter.drawLine((int) v.x, (int) v.y,
            (int) (v.x + t.x), (int) (v.y + t.y));

    painter.setPen(Qt::white);
    painter.drawLine((int) v.x, (int) v.y,
            (int) (v.x + n.x), (int) (v.y + n.y));

    painter.setPen(Qt::cyan);
    painter.setBrush(QBrush(Qt::cyan));
    painter.drawEllipse((int) v.x - POINT_RADIUS,
            (int) v.y - POINT_RADIUS, POINT_RADIUS * 2 + 1,
            POINT_RADIUS * 2);

}

void Cubic2DWidget::setCubicType(CubicType cubicType) {
    if (this->cubicType == cubicType) {
        return;
    }
    this->cubicType = cubicType;

    switch (cubicType) {
        case CubicType::BEZIER: {
            baseMatrix = rev::BaseMatrix<scalar_t>::BEZIER;
            break;
        }
        case CubicType::BSPLINE: {
            baseMatrix = rev::BaseMatrix<scalar_t>::BSPLINE;
            break;
        }
        case CubicType::CATMULL_ROM: {
            baseMatrix = rev::BaseMatrix<scalar_t>::CATMULL_ROM;
            break;
        }
        case CubicType::HERMITE: {
            baseMatrix = rev::BaseMatrix<scalar_t>::HERMITE;
            break;
        }
    }

    clear();
}

void Cubic2DWidget::clearAction() {
    clear();
}

void Cubic2DWidget::curveTypeChangedAction(int index) {
    setCubicType((CubicType) index);
}

void Cubic2DWidget::drawNormalsSelected(bool selected) {
    mainWindow->cubic2DnormalSlider->setEnabled(selected);
    setDrawNormals(selected);
}

void Cubic2DWidget::drawNormalsSliderValueChanged(int value) {
    setNormalPosition((double) value / 100.0);
}

void Cubic2DWidget::setShowControlLines(bool showControlLines) {
    this->showControlLines = showControlLines;
    repaint();
}

void Cubic2DWidget::setShowControlPoints(bool showControlPoints) {
    this->showControlPoints = showControlPoints;
    repaint();
}

