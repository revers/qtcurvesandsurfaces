/*
 * BSplineWidget.h
 *
 *  Created on: 21-04-2013
 *      Author: Revers
 */

#ifndef BSPLINEWIDGET_H_
#define BSPLINEWIDGET_H_

#include <iostream>
#include <vector>

#include <QWidget>
#include <QSize>
#include <QColor>

#include "QtCurvesAndSurfacesConfig.h"

class QMouseEvent;
class QPainter;

namespace Ui {
	class MainWindow;
}

namespace rev {

	class BSplineWidget: public QWidget {
	Q_OBJECT

		static const int POINT_RADIUS = 4;

		std::vector<vec2_t> points;
		scalar_t tension = scalar_t(3);
		scalar_t baseMatrix[4][4];

		QColor bgColor;
		bool showControlLines = true;
		bool showControlPoints = true;
		bool mousePressed = false;
		bool useKnotVector = false;

		int focusPointIndex = -1;

		Ui::MainWindow* mainWindow;

	public:
		BSplineWidget(Ui::MainWindow* mainWindow,
				QWidget* parent = 0);

		virtual ~BSplineWidget();

		void clear();

		bool isShowControlLines() const {
			return showControlLines;
		}

		bool isShowControlPoints() const {
			return showControlPoints;
		}

	protected:
		void paintEvent(QPaintEvent* event) override;
		void mousePressEvent(QMouseEvent* event) override;
		void mouseReleaseEvent(QMouseEvent* event) override;
		void mouseMoveEvent(QMouseEvent* event) override;

	private:
		bool hitPoint(const QPoint& p);
		void moveFocusedPoint(const QPoint& p);

		void setupActions();
		void drawControlPoints(QPainter* p);
		void drawClosingSegments(QPainter* p);
		void drawSegment(QPainter& painter, const vec2_t& p1, const vec2_t& p2,
				const vec2_t& p3, const vec2_t& p4);

		virtual QSize sizeHint() const override {
			return QSize(300, 100);
		}
		void applyTension();
		void applyKnotVector(const vec2_t& p1, const vec2_t& p2, const vec2_t& p3,
				const vec2_t& p4);

	private slots:
		void clearAction();
		void closedCurveChecked(bool checked);
		void drawJointPointsChecked(bool checked);
		void tensionValueChanged(double val);
	};

} /* namespace rev */
#endif /* BSPLINEWIDGET_H_ */
