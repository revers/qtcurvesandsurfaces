/*
 * HermiteInterpolationWidget.h
 *
 *  Created on: 16-02-2013
 *      Author: Revers
 */

#ifndef HERMITEINTERPOLATIONWIDGET_H_
#define HERMITEINTERPOLATIONWIDGET_H_

#include <iostream>
#include <vector>

#include <QWidget>
#include <QSize>
#include <QColor>

#include "QtCurvesAndSurfacesConfig.h"

class QMouseEvent;
class QPainter;

namespace Ui {
    class MainWindow;
}

namespace rev {

    class HermiteInterpolationWidget: public QWidget {
    Q_OBJECT

        static const int POINT_RADIUS = 4;
        std::vector<vec2_t> points;

        QColor bgColor;
        bool showTangentLines = true;
        bool showControlPoints = true;
        bool mousePressed = false;

        int focusPointIndex = -1;

        Ui::MainWindow* mainWindow;

    public:
        HermiteInterpolationWidget(Ui::MainWindow* mainWindow,
                QWidget* parent = 0);

        virtual ~HermiteInterpolationWidget();

        void clear();

        bool hitPoint(const QPoint& p);

        void moveFocusedPoint(const QPoint& p);

        bool isShowTangentLines() const {
            return showTangentLines;
        }

        void setShowTangentLines(bool showTangentLines) {
            this->showTangentLines = showTangentLines;
        }

    protected:
        void paintEvent(QPaintEvent* event) override;
        void mousePressEvent(QMouseEvent* event) override;
        void mouseReleaseEvent(QMouseEvent* event) override;
        void mouseMoveEvent(QMouseEvent* event) override;

    private:
        void setupActions();
        void drawControlPoints(QPainter* p);
        void drawTangentLines(QPainter& p);

        virtual QSize sizeHint() const override {
            return QSize(300, 100);
        }

    private slots:
        void clearAction();
    };

} /* namespace rev */

#endif /* HERMITEINTERPOLATIONWIDGET_H_ */
