/*
 * File:   QtCurvesAndSurfacesWindow.cpp
 * Author: Kamil
 *
 * Created on 13 czerwiec 2012, 13:32
 */

#include <iostream>

#include <QEvent>
#include <QTimer>
#include <QDebug>
#include <QString>
#include <QMessageBox>
#include <QMouseEvent>
#include <QAbstractEventDispatcher>
#include "QtCurvesAndSurfacesWindow.h"
#include "LagrangePolynomialWidget.h"
#include "HermiteInterpolationWidget.h"
#include "CardinalSplineWidget.h"
#include "BezierCurveWidget.h"
#include "BSplineWidget.h"

using namespace std;
using namespace rev;

#ifdef NDEBUG
#define WINDOW_TITLE "QtCurvesAndSurfaces"
#else
#define WINDOW_TITLE "QtCurvesAndSurfaces (DEBUG)"
#endif

QtCurvesAndSurfacesWindow::QtCurvesAndSurfacesWindow() {
	widget.setupUi(this);

	cubic2DWidget = new Cubic2DWidget(&widget, this);
	lagrangePolyWidget = new LagrangePolynomialWidget(&widget, this);
	hermiteInterpWidget = new HermiteInterpolationWidget(&widget, this);
	cardinalSplineWidget = new CardinalSplineWidget(&widget, this);
	bezierCurveWidget = new BezierCurveWidget(&widget, this);
	bsplineWidget = new BSplineWidget(&widget, this);

	widget.cubic2DLayout->addWidget(cubic2DWidget);
	widget.cubic2DLayout->setStretchFactor(cubic2DWidget, 0x7FFFFFFF);
	cubic2DWidget->setSizePolicy(QSizePolicy::Expanding,
			QSizePolicy::Expanding);

	widget.lagrangePolyLayout->addWidget(lagrangePolyWidget);
	widget.lagrangePolyLayout->setStretchFactor(lagrangePolyWidget, 0x7FFFFFFF);
	lagrangePolyWidget->setSizePolicy(QSizePolicy::Expanding,
			QSizePolicy::Expanding);

	widget.hermiteInterpLayout->addWidget(hermiteInterpWidget);
	widget.hermiteInterpLayout->setStretchFactor(hermiteInterpWidget, 0x7FFFFFFF);
	hermiteInterpWidget->setSizePolicy(QSizePolicy::Expanding,
			QSizePolicy::Expanding);

	widget.cardSplineLayout->addWidget(cardinalSplineWidget);
	widget.cardSplineLayout->setStretchFactor(cardinalSplineWidget, 0x7FFFFFFF);
	cardinalSplineWidget->setSizePolicy(QSizePolicy::Expanding,
			QSizePolicy::Expanding);

	widget.bezierLayout->addWidget(bezierCurveWidget);
	widget.bezierLayout->setStretchFactor(bezierCurveWidget, 0x7FFFFFFF);
	bezierCurveWidget->setSizePolicy(QSizePolicy::Expanding,
			QSizePolicy::Expanding);

	widget.bsplineLayout->addWidget(bsplineWidget);
	widget.bsplineLayout->setStretchFactor(bsplineWidget, 0x7FFFFFFF);
	bsplineWidget->setSizePolicy(QSizePolicy::Expanding,
			QSizePolicy::Expanding);

	setupActions();

	resize(1024, 750);
}

QtCurvesAndSurfacesWindow::~QtCurvesAndSurfacesWindow() {
}

void QtCurvesAndSurfacesWindow::setupActions() {
	connect(widget.actionExit, SIGNAL(triggered(bool)), qApp, SLOT(quit()));

	connect(widget.actionAbout, SIGNAL(triggered(bool)),
			this, SLOT(aboutAction()));
}

void QtCurvesAndSurfacesWindow::aboutAction() {
	QMessageBox::information(this, "About...", "Author: Kamil Kolaczynski");
}
