/*
 * BezierCurveWidget.cpp
 *
 *  Created on: 22-02-2013
 *      Author: Revers
 */

#include "BezierCurveWidget.h"

#include <iostream>
#include <QApplication>
#include <QPainter>
#include <QButtonGroup>
#include <QMouseEvent>
#include <QRect>
//#include <rev/common/RevAssert.h>

#include "ui_QtCurvesAndSurfacesWindow.h"
#include "../rev/RevCubicCurve.h"

using namespace rev;
using namespace std;

typedef TCubicCurve2D<scalar_t> CubicCurve2D;
typedef TCubicBaseMatrix<scalar_t> CubicBaseMatrix;

BezierCurveWidget::BezierCurveWidget(Ui::MainWindow* mainWindow,
		QWidget* parent) :
		bgColor(203, 214, 230) {
	this->mainWindow = mainWindow;

	QButtonGroup* group = new QButtonGroup(this);
	group->addButton(mainWindow->bezierMethodCB);
	group->addButton(mainWindow->deCasteljauMethodCB);

	setupActions();
}

BezierCurveWidget::~BezierCurveWidget() {
}

void BezierCurveWidget::setupActions() {
	connect(mainWindow->bezierClearB, SIGNAL(clicked()),
			this, SLOT(clearAction()));

    connect(mainWindow->bezierMethodCB, SIGNAL(toggled(bool)),
            this, SLOT(bezierMethodChecked(bool)));

    connect(mainWindow->deCasteljauMethodCB, SIGNAL(toggled(bool)),
            this, SLOT(deCasteljauMethodChecked(bool)));
}

void BezierCurveWidget::clear() {
	points.clear();
	focusPointIndex = -1;
	mainWindow->bezierPointsL->setText(QString("Points: 0"));
	repaint();
}

void BezierCurveWidget::mousePressEvent(QMouseEvent* event) {
	if (event->button() == Qt::LeftButton) {
		QPoint p = event->pos();
		mousePressed = true;

		if (!hitPoint(p)) {
			focusPointIndex = points.size();
			points.push_back(vec2_t(p.x(), p.y()));
			mainWindow->bezierPointsL->setText(
					QString("Points: %1").arg(points.size()));
		}
	}

	repaint();
}

void BezierCurveWidget::mouseReleaseEvent(QMouseEvent* event) {
	mousePressed = false;
	focusPointIndex = -1;
	repaint();
}

void BezierCurveWidget::mouseMoveEvent(QMouseEvent* event) {
	if (!mousePressed) {
		return;
	}

	moveFocusedPoint(event->pos());
	repaint();
}

bool BezierCurveWidget::hitPoint(const QPoint& p) {
	int index = 0;
	for (vec2_t& v : points) {
		if (glm::distance(vec2_t(p.x(), p.y()), v) <= POINT_RADIUS + 2) {
			focusPointIndex = index;
			return true;
		}
		index++;
	}
	focusPointIndex = -1;
	return false;
}

void BezierCurveWidget::moveFocusedPoint(const QPoint& p) {
	if (focusPointIndex < 0) {
		return;
	}

	points[focusPointIndex] = vec2_t(p.x(), p.y());
}

/**
 * De Casteljau method.
 */
vec2_t mediation(scalar_t t, vec2_t* points, int n) {
	if (n == 2) {
		return (1.0 - t) * points[0] + t * points[1];
	}
	return (1.0 - t) * mediation(t, points, n - 1) + t * mediation(t, points + 1, n - 1);
}

void BezierCurveWidget::paintEvent(QPaintEvent* event) {
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing, true);

	painter.setPen(bgColor);
	painter.setBrush(QBrush(bgColor));
	painter.drawRect(0, 0, width() - 1, height() - 1);

	for (int i = 0; i < points.size()
			&& points.size() - i >= 4; i += 3) {

		const vec2_t& p1 = points[i];
		const vec2_t& p2 = points[i + 1];
		const vec2_t& p3 = points[i + 2];
		const vec2_t& p4 = points[i + 3];

		if (useDeCasteljau) {
			painter.setPen(Qt::green);
			for (scalar_t t = 0; t < 1; t += 0.01) {
				vec2_t v1 = mediation(t, &points[i], 4);
				vec2_t v2 = mediation(t + 0.01, &points[i], 4);

				painter.drawLine((int) v1.x, (int) v1.y,
						(int) v2.x, (int) v2.y);
			}
		} else {
			painter.setPen(Qt::red);
			CubicCurve2D curve(CubicBaseMatrix::BEZIER,
					p1, p2, p3, p4);

			for (scalar_t t = 0; t < 1; t += 0.01) {
				vec2_t v1 = curve.eval(t);
				vec2_t v2 = curve.eval(t + 0.01);

				painter.drawLine((int) v1.x, (int) v1.y,
						(int) v2.x, (int) v2.y);
			}
		}

		if (showControlLines) {
			painter.setPen(QColor(180, 180, 180));

			painter.drawLine(p1.x, p1.y, p2.x, p2.y);
			painter.drawLine(p3.x, p3.y, p4.x, p4.y);
		}
	}

	if (showControlPoints) {
		drawControlPoints(&painter);
	}
}

void BezierCurveWidget::drawControlPoints(QPainter* p) {
	QPainter& painter = *p;

	for (int i = 0; i < points.size(); i++) {
		if (i == focusPointIndex) {
			painter.setBrush(QBrush(Qt::red));
			painter.setPen(Qt::red);
		} else {
			painter.setBrush(QBrush(Qt::black));
			painter.setPen(Qt::black);
		}

		const vec2_t& v = points[i];
		painter.drawEllipse((int) v.x - POINT_RADIUS,
				(int) v.y - POINT_RADIUS, POINT_RADIUS * 2 + 1,
				POINT_RADIUS * 2);
	}
}

void BezierCurveWidget::clearAction() {
	clear();
}

void BezierCurveWidget::bezierMethodChecked(bool checked) {
	useDeCasteljau = !checked;
	repaint();
}

void BezierCurveWidget::deCasteljauMethodChecked(bool checked) {
	useDeCasteljau = checked;
	repaint();
}
