/*
 * QtCurvesAndSurfacesConfig.h
 *
 *  Created on: 17-02-2013
 *      Author: Revers
 */

#ifndef QTCURVESANDSURFACESCONFIG_H_
#define QTCURVESANDSURFACESCONFIG_H_

#include <glm/glm.hpp>

#define USE_DOUBLE

#ifdef USE_DOUBLE
typedef double scalar_t;
#else
typedef float scalar_t;
#endif

typedef glm::detail::tvec2<scalar_t> vec2_t;
typedef glm::detail::tmat4x4<scalar_t> mat4_t;

#endif /* QTCURVESANDSURFACESCONFIG_H_ */
