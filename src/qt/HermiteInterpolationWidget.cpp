/*
 * HermiteInterpolationWidget.cpp
 *
 *  Created on: 16-02-2013
 *      Author: Revers
 */

#include "HermiteInterpolationWidget.h"

#include <iostream>
#include <QApplication>
#include <QPainter>
#include <QMouseEvent>
#include <QRect>

#include "ui_QtCurvesAndSurfacesWindow.h"
#include "../rev/RevCubicCurve.h"

using namespace rev;
using namespace std;

typedef TCubicCurve2D<scalar_t> CubicCurve2D;
typedef TCubicBaseMatrix<scalar_t> CubicBaseMatrix;

HermiteInterpolationWidget::HermiteInterpolationWidget(Ui::MainWindow* mainWindow,
        QWidget* parent) :
        bgColor(203, 214, 230) {
    this->mainWindow = mainWindow;

    setupActions();
}

HermiteInterpolationWidget::~HermiteInterpolationWidget() {
}

void HermiteInterpolationWidget::setupActions() {
    connect(mainWindow->hermiteInterpClearB, SIGNAL(clicked()),
            this, SLOT(clearAction()));
}

void HermiteInterpolationWidget::clear() {
    points.clear();
    focusPointIndex = -1;
    mainWindow->hermiteInterpPointsL->setText(QString("Points: 0"));
    repaint();
}

void HermiteInterpolationWidget::mousePressEvent(QMouseEvent* event) {
    if (event->button() == Qt::LeftButton) {
        QPoint p = event->pos();
        mousePressed = true;

        if (!hitPoint(p)) {
            focusPointIndex = points.size();
            points.push_back(vec2_t(p.x(), p.y()));
            mainWindow->hermiteInterpPointsL->setText(
                    QString("Points: %1").arg(points.size()));
        }
    }

    repaint();
}

void HermiteInterpolationWidget::mouseReleaseEvent(QMouseEvent* event) {
    mousePressed = false;
    focusPointIndex = -1;
    repaint();
}

void HermiteInterpolationWidget::mouseMoveEvent(QMouseEvent* event) {
    if (!mousePressed) {
        return;
    }

    moveFocusedPoint(event->pos());
    repaint();
}

bool HermiteInterpolationWidget::hitPoint(const QPoint& p) {
    int index = 0;
    for (vec2_t& v : points) {
        if (glm::distance(vec2_t(p.x(), p.y()), v) <= POINT_RADIUS + 2) {
            focusPointIndex = index;
            return true;
        }
        index++;
    }
    focusPointIndex = -1;
    return false;
}

void HermiteInterpolationWidget::moveFocusedPoint(const QPoint& p) {
    if (focusPointIndex < 0) {
        return;
    }

    points[focusPointIndex] = vec2_t(p.x(), p.y());
}

void HermiteInterpolationWidget::drawTangentLines(QPainter& p) {
}

void HermiteInterpolationWidget::paintEvent(QPaintEvent* event) {
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    painter.setPen(bgColor);
    painter.setBrush(QBrush(bgColor));
    painter.drawRect(0, 0, width() - 1, height() - 1);

    for (int i = 0; i < points.size()
            && points.size() - i >= 4; i += 3) {
        const vec2_t& p1 = points[i];
        const vec2_t& p2 = points[i + 1];
        const vec2_t& p3 = points[i + 2];
        const vec2_t& p4 = points[i + 3];

        scalar_t factor(4);
        vec2_t d1 = (p3 - p1) * factor;
        vec2_t d2 = (p2 - p4) * factor;

        CubicCurve2D curve(CubicBaseMatrix::HERMITE,
                p1, p2, d1, d2);

        painter.setPen(Qt::red);

        for (scalar_t t = 0; t < 1; t += 0.01) {
            vec2_t v1 = curve.eval(t);
            vec2_t v2 = curve.eval(t + 0.01);

            painter.drawLine((int) v1.x, (int) v1.y,
                    (int) v2.x, (int) v2.y);
        }

        if (showTangentLines) {
            painter.setPen(QColor(180, 180, 180));

            painter.drawLine(p1.x, p1.y, p3.x, p3.y);
            painter.drawLine(p2.x, p2.y, p4.x, p4.y);
        }
    }

    if (showControlPoints) {
        drawControlPoints(&painter);
    }
}

void HermiteInterpolationWidget::drawControlPoints(QPainter* p) {
    QPainter& painter = *p;

    for (int i = 0; i < points.size(); i++) {
        if (i == focusPointIndex) {
            painter.setBrush(QBrush(Qt::red));
            painter.setPen(Qt::red);
//        else if (i % 3 == 0) {
//            painter.setBrush(QBrush(Qt::blue));
//            painter.setPen(Qt::blue);
        } else {
            painter.setBrush(QBrush(Qt::black));
            painter.setPen(Qt::black);
        }

        const vec2_t& v = points[i];
        painter.drawEllipse((int) v.x - POINT_RADIUS,
                (int) v.y - POINT_RADIUS, POINT_RADIUS * 2 + 1,
                POINT_RADIUS * 2);
    }
}

void HermiteInterpolationWidget::clearAction() {
    clear();
}

