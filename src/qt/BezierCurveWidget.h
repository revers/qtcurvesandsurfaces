/*
 * BezierCurveWidget.h
 *
 *  Created on: 22-02-2013
 *      Author: Revers
 */

#ifndef BEZIERCURVEWIDGET_H_
#define BEZIERCURVEWIDGET_H_

#include <iostream>
#include <vector>

#include <QWidget>
#include <QSize>
#include <QColor>

#include "QtCurvesAndSurfacesConfig.h"

class QMouseEvent;
class QPainter;

namespace Ui {
	class MainWindow;
}

namespace rev {

	class BezierCurveWidget: public QWidget {
	Q_OBJECT

		static const int POINT_RADIUS = 4;

		std::vector<vec2_t> points;

		QColor bgColor;
		bool showControlLines = true;
		bool showControlPoints = true;
		bool mousePressed = false;
		bool useDeCasteljau = false;

		int focusPointIndex = -1;

		Ui::MainWindow* mainWindow;

	public:
		BezierCurveWidget(Ui::MainWindow* mainWindow,
				QWidget* parent = 0);

		virtual ~BezierCurveWidget();

		void clear();

		bool isShowControlLines() const {
			return showControlLines;
		}

		bool isShowControlPoints() const {
			return showControlPoints;
		}

	protected:
		void paintEvent(QPaintEvent* event) override;
		void mousePressEvent(QMouseEvent* event) override;
		void mouseReleaseEvent(QMouseEvent* event) override;
		void mouseMoveEvent(QMouseEvent* event) override;

	private:
		bool hitPoint(const QPoint& p);
		void moveFocusedPoint(const QPoint& p);

		void setupActions();
		void drawControlPoints(QPainter* p);

		virtual QSize sizeHint() const override {
			return QSize(300, 100);
		}

	private slots:
		void clearAction();
		void bezierMethodChecked(bool checked);
		void deCasteljauMethodChecked(bool checked);
	};

} /* namespace rev */

#endif /* BEZIERCURVEWIDGET_H_ */
