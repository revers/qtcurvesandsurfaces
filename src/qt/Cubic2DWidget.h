/*
 * Cubic2DWidget.h
 *
 *  Created on: 22-11-2012
 *      Author: Revers
 */

#ifndef CUBIC2DWIDGET_H_
#define CUBIC2DWIDGET_H_

#include <iostream>
#include <vector>

#include <QWidget>
#include <QSize>
#include <QColor>

#include "QtCurvesAndSurfacesConfig.h"
#include "../RevCubic2D.hpp"

class QMouseEvent;
class QPainter;

namespace Ui {
    class MainWindow;
}

namespace rev {

    typedef TCubic2D<scalar_t> Cubic2D;

    enum class CubicType {
        BEZIER, HERMITE, BSPLINE, CATMULL_ROM
    };

    class Cubic2DWidget: public QWidget {
    Q_OBJECT

        static const int POINT_RADIUS = 4;

        std::vector<vec2_t> points;
        mat4_t baseMatrix;
        CubicType cubicType;

        QColor bgColor;
        bool showControlLines = true;
        bool showControlPoints = true;
        bool mousePressed = false;
        bool drawNormals = false;

        scalar_t normalPosition = 0.0;

        int focusPointIndex = -1;

        Ui::MainWindow* mainWindow;

    public:
        Cubic2DWidget(Ui::MainWindow* mainWindow,
                QWidget* parent = 0);

        virtual ~Cubic2DWidget();

        void setCubicType(CubicType cubicType);

        void clear() {
            points.clear();
            focusPointIndex = -1;
            repaint();
        }

        bool isShowControlLines() const {
            return showControlLines;
        }

        bool isShowControlPoints() const {
            return showControlPoints;
        }

        bool hitPoint(const QPoint& p);

        void moveFocusedPoint(const QPoint& p);

        bool isDrawNormals() const {
            return drawNormals;
        }

        void setDrawNormals(bool drawNormals) {
            this->drawNormals = drawNormals;
            repaint();
        }

        scalar_t getNormalPosition() const {
            return normalPosition;
        }

        void setNormalPosition(scalar_t normalPosition) {
            this->normalPosition = normalPosition;
            repaint();
        }

    protected:
        void paintEvent(QPaintEvent* event) override;
        void mousePressEvent(QMouseEvent* event) override;
        void mouseReleaseEvent(QMouseEvent* event) override;
        void mouseMoveEvent(QMouseEvent* event) override;

    private:
        void setupActions();
        void drawControlPoints(QPainter* p);
        void drawControlLines(QPainter* p);

        void drawNormal(QPainter* p, Cubic2D& curve, scalar_t pos);

        virtual QSize sizeHint() const override {
            return QSize(300, 100);
        }

    private slots:
        void clearAction();
        void curveTypeChangedAction(int index);
        void drawNormalsSelected(bool selected);
        void drawNormalsSliderValueChanged(int value);

        void setShowControlLines(bool showControlLines);

        void setShowControlPoints(bool showControlPoints);
    };

} /* namespace rev */
#endif /* CUBIC2DWIDGET_H_ */
