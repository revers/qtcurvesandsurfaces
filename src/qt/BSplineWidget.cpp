/*
 * BSplineWidget.cpp
 *
 *  Created on: 21-04-2013
 *      Author: Revers
 */

#include "BSplineWidget.h"

#include <iostream>
#include <QApplication>
#include <QPainter>
#include <QButtonGroup>
#include <QMouseEvent>
#include <QRect>
#include <rev/common/RevErrorStream.h>
#include <rev/common/RevAssert.h>

#include "ui_QtCurvesAndSurfacesWindow.h"
#include "../rev/RevCubicCurve.h"

using namespace rev;
using namespace std;

typedef TCubicCurve2D<scalar_t> CubicCurve2D;
typedef TCubicBaseMatrix<scalar_t> CubicBaseMatrix;

BSplineWidget::BSplineWidget(Ui::MainWindow* mainWindow,
		QWidget* parent) :
		bgColor(203, 214, 230) {
	this->mainWindow = mainWindow;

//	QButtonGroup* group = new QButtonGroup(this);
//	group->addButton(mainWindow->bsplineMethodCB);
//	group->addButton(mainWindow->deCasteljauMethodCB);

	applyTension();
	setupActions();
}

BSplineWidget::~BSplineWidget() {
}

void BSplineWidget::setupActions() {
	connect(mainWindow->bsplineClearB, SIGNAL(clicked()),
			this, SLOT(clearAction()));

	connect(mainWindow->bsplineClosedCurveCB, SIGNAL(toggled(bool)),
			this, SLOT(closedCurveChecked(bool)));

	connect(mainWindow->bsplineJointPointsCB, SIGNAL(toggled(bool)),
			this, SLOT(drawJointPointsChecked(bool)));

	connect(mainWindow->bsplineTensionSB, SIGNAL(valueChanged(double)),
			this, SLOT(tensionValueChanged(double)));
}

void BSplineWidget::clear() {
	points.clear();
	focusPointIndex = -1;
	mainWindow->bsplinePointsL->setText(QString("Points: 0"));
	repaint();
}

void BSplineWidget::mousePressEvent(QMouseEvent* event) {
	if (event->button() == Qt::LeftButton) {
		QPoint p = event->pos();
		mousePressed = true;

		if (!hitPoint(p)) {
			focusPointIndex = points.size();
			points.push_back(vec2_t(p.x(), p.y()));
			mainWindow->bsplinePointsL->setText(
					QString("Points: %1").arg(points.size()));
		}
	}

	repaint();
}

void BSplineWidget::mouseReleaseEvent(QMouseEvent* event) {
	mousePressed = false;
	focusPointIndex = -1;
	repaint();
}

void BSplineWidget::mouseMoveEvent(QMouseEvent* event) {
	if (!mousePressed) {
		return;
	}

	moveFocusedPoint(event->pos());
	repaint();
}

bool BSplineWidget::hitPoint(const QPoint& p) {
	int index = 0;
	for (vec2_t& v : points) {
		if (glm::distance(vec2_t(p.x(), p.y()), v) <= POINT_RADIUS + 2) {
			focusPointIndex = index;
			return true;
		}
		index++;
	}
	focusPointIndex = -1;
	return false;
}

void BSplineWidget::moveFocusedPoint(const QPoint& p) {
	if (focusPointIndex < 0) {
		return;
	}

	points[focusPointIndex] = vec2_t(p.x(), p.y());
}

void BSplineWidget::drawSegment(QPainter& painter, const vec2_t& p1, const vec2_t& p2,
		const vec2_t& p3, const vec2_t& p4) {
	useKnotVector = false;
	if (useKnotVector) {
		applyKnotVector(p1, p2, p3, p4);
		CubicCurve2D curve = CubicCurve2D(baseMatrix, p1, p2, p3, p4);
		for (scalar_t t = 0; t < 1; t += 0.01) {
			vec2_t v1 = curve.eval(t);
			vec2_t v2 = curve.eval(t + 0.01);

			painter.drawLine((int) v1.x, (int) v1.y,
					(int) v2.x, (int) v2.y);
		}
	} else {
		CubicCurve2D curve = CubicCurve2D(baseMatrix, p1, p2, p3, p4);
		for (scalar_t t = 0; t < 1; t += 0.01) {
			vec2_t v1 = curve.eval(t);
			vec2_t v2 = curve.eval(t + 0.01);

			painter.drawLine((int) v1.x, (int) v1.y,
					(int) v2.x, (int) v2.y);
		}
	}
}

void BSplineWidget::drawClosingSegments(QPainter* p) {
	QPainter& painter = *p;

	vec2_t p1 = points[points.size() - 1];
	vec2_t p2 = points[0];
	vec2_t p3 = points[1];
	vec2_t p4 = points[2];

	painter.setPen(Qt::green);
	drawSegment(painter, p1, p2, p3, p4);

	p1 = points[points.size() - 2];
	p2 = points[points.size() - 1];
	p3 = points[0];
	p4 = points[1];

	painter.setPen(Qt::blue);
	drawSegment(painter, p1, p2, p3, p4);

	p1 = points[points.size() - 3];
	p2 = points[points.size() - 2];
	p3 = points[points.size() - 1];
	p4 = points[0];

	painter.setPen(Qt::magenta);
	drawSegment(painter, p1, p2, p3, p4);

	if (mainWindow->bsplineJointPointsCB->isChecked()) {
		vec2_t kStart = scalar_t(1.0 / 6.0)
				* ((points[points.size() - 1]) + 4.0 * (points[0]) + (points[1]));
		painter.setPen(Qt::yellow);
		painter.drawEllipse((int) kStart.x - POINT_RADIUS,
				(int) kStart.y - POINT_RADIUS, POINT_RADIUS * 2 + 1,
				POINT_RADIUS * 2);

		kStart = scalar_t(1.0 / 6.0)
				* ((points[points.size() - 2]) + 4.0 * (points[points.size() - 1]) + (points[0]));
		painter.setPen(Qt::yellow);
		painter.drawEllipse((int) kStart.x - POINT_RADIUS,
				(int) kStart.y - POINT_RADIUS, POINT_RADIUS * 2 + 1,
				POINT_RADIUS * 2);

		kStart = scalar_t(1.0 / 6.0)
				* ((points[points.size() - 3]) + 4.0 * (points[points.size() - 2])
						+ (points[points.size() - 1]));
		painter.setPen(Qt::yellow);
		painter.drawEllipse((int) kStart.x - POINT_RADIUS,
				(int) kStart.y - POINT_RADIUS, POINT_RADIUS * 2 + 1,
				POINT_RADIUS * 2);
	}

}

void BSplineWidget::paintEvent(QPaintEvent* event) {
	QPainter painter(this);
	painter.setRenderHint(QPainter::Antialiasing, true);

	painter.setPen(bgColor);
	painter.setBrush(QBrush(bgColor));
	painter.drawRect(0, 0, width() - 1, height() - 1);

	for (int i = 0; i < points.size()
			&& points.size() - i >= 4; i++) {

		const vec2_t& p1 = points[i];
		const vec2_t& p2 = points[i + 1];
		const vec2_t& p3 = points[i + 2];
		const vec2_t& p4 = points[i + 3];

		painter.setPen(Qt::red);
		drawSegment(painter, p1, p2, p3, p4);

		if (showControlLines) {
			painter.setPen(QColor(180, 180, 180));

			painter.drawLine(p1.x, p1.y, p2.x, p2.y);
			painter.drawLine(p3.x, p3.y, p4.x, p4.y);
		}

		if (mainWindow->bsplineJointPointsCB->isChecked()) {
			vec2_t kStart = scalar_t(1.0 / 6.0) * (p1 + 4.0 * p2 + p3);
			painter.setPen(Qt::yellow);
			painter.drawEllipse((int) kStart.x - POINT_RADIUS,
					(int) kStart.y - POINT_RADIUS, POINT_RADIUS * 2 + 1,
					POINT_RADIUS * 2);
		}
	}
	if (mainWindow->bsplineClosedCurveCB->isChecked() && points.size() >= 4) {
		drawClosingSegments(&painter);
	}

	if (showControlPoints) {
		drawControlPoints(&painter);
	}
}

void BSplineWidget::drawControlPoints(QPainter* p) {
	QPainter& painter = *p;

	for (int i = 0; i < points.size(); i++) {
		if (i == focusPointIndex) {
			painter.setBrush(QBrush(Qt::red));
			painter.setPen(Qt::red);
		} else {
			painter.setBrush(QBrush(Qt::black));
			painter.setPen(Qt::black);
		}

		const vec2_t& v = points[i];
		painter.drawEllipse((int) v.x - POINT_RADIUS,
				(int) v.y - POINT_RADIUS, POINT_RADIUS * 2 + 1,
				POINT_RADIUS * 2);
	}
}

void BSplineWidget::clearAction() {
	clear();
}

void BSplineWidget::closedCurveChecked(bool checked) {
	//REV_TRACE_MSG(R(checked));
	repaint();
}

void BSplineWidget::drawJointPointsChecked(bool checked) {
	//REV_TRACE_MSG(R(checked));
	repaint();
}

void BSplineWidget::applyTension() {
	scalar_t a(1.0 / 6.0);
	scalar_t& s = tension;
	baseMatrix[0][0] = a * (2.0 - s);
	baseMatrix[0][1] = a * (6.0 - s);
	baseMatrix[0][2] = a * (s - 6.0);
	baseMatrix[0][3] = a * (s - 2.0);

	baseMatrix[1][0] = a * (2.0 * s - 3.0);
	baseMatrix[1][1] = a * (s - 9.0);
	baseMatrix[1][2] = a * (9.0 - 2.0 * s);
	baseMatrix[1][3] = a * (3.0 - s);

	baseMatrix[2][0] = a * (-s);
	baseMatrix[2][1] = 0;
	baseMatrix[2][2] = a * s;
	baseMatrix[2][3] = 0;

	baseMatrix[3][0] = a * 1.0;
	baseMatrix[3][1] = a * 4.0;
	baseMatrix[3][2] = a * 1.0;
	baseMatrix[3][3] = 0;
}

#define SQR(a) ((a)*(a))

void BSplineWidget::applyKnotVector(const vec2_t& p1, const vec2_t& p2, const vec2_t& p3,
		const vec2_t& p4) {
	scalar_t len1 = glm::length(p2 - p1);
	scalar_t len2 = glm::length(p3 - p2);
	scalar_t len3 = glm::length(p4 - p3);

	scalar_t del1 = 0;
	scalar_t del2 = 0;
	scalar_t del3 = 1;
	scalar_t del4 = 2;
	scalar_t del5 = 2;

	scalar_t a = SQR(del3) / ((del1 + del2 + del3) * (del2 + del3));
	scalar_t b = SQR(del3) / ((del2 + del3 + del4) * (del2 + del3));
	scalar_t c = SQR(del3) / ((del2 + del3 + del4) * (del3 + del4));

	scalar_t d = SQR(del3) / ((del3 + del4 + del5) * (del4 + del5));
	scalar_t e = (del2 * del3) / ((del2 + del3 + del4) * (del2 + del3));
	scalar_t f = SQR(del2) / ((del2 + del3 + del4) * (del2 + del3));

	baseMatrix[0][0] = -a;
	baseMatrix[0][1] = a + b + c;
	baseMatrix[0][2] = -b - c - d;
	baseMatrix[0][3] = d;

	baseMatrix[1][0] = 3.0 * a;
	baseMatrix[1][1] = -3.0 * a - 3.0 * b;
	baseMatrix[1][2] = 3.0 * b;
	baseMatrix[1][3] = 0.0;

	baseMatrix[2][0] = -3.0 * a;
	baseMatrix[2][1] = 3.0 * a - 3.0 * e;
	baseMatrix[2][2] = 3.0 * e;
	baseMatrix[2][3] = 0.0;

	baseMatrix[3][0] = a;
	baseMatrix[3][1] = 1.0 - a - f;
	baseMatrix[3][2] = f;
	baseMatrix[3][3] = 0.0;
}

void BSplineWidget::tensionValueChanged(double val) {
	tension = val;
	applyTension();
	repaint();
}
