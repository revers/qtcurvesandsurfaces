/*
 * CardinalSplineWidget.h
 *
 *  Created on: 20-02-2013
 *      Author: Revers
 */

#ifndef CARDINALSPLINEWIDGET_H_
#define CARDINALSPLINEWIDGET_H_

#include <iostream>
#include <vector>

#include <QWidget>
#include <QSize>
#include <QColor>

#include "QtCurvesAndSurfacesConfig.h"

class QMouseEvent;
class QPainter;

namespace Ui {
    class MainWindow;
}

namespace rev {

    class CardinalSplineWidget: public QWidget {
    Q_OBJECT

        static const int POINT_RADIUS = 4;
        std::vector<vec2_t> points;

        QColor bgColor;
        bool showTangentLines = true;
        bool showControlPoints = true;
        bool mousePressed = false;
        bool useCatmullRom = false;

        int focusPointIndex = -1;
        int tangentFactor = 100;
        double catmullRomFactor = 0.5;

        Ui::MainWindow* mainWindow;

    public:
        CardinalSplineWidget(Ui::MainWindow* mainWindow,
                QWidget* parent = 0);

        virtual ~CardinalSplineWidget();

        void clear();

        bool hitPoint(const QPoint& p);

        void moveFocusedPoint(const QPoint& p);

        bool isShowTangentLines() const {
            return showTangentLines;
        }

        void setShowTangentLines(bool showTangentLines) {
            this->showTangentLines = showTangentLines;
        }

    protected:
        void paintEvent(QPaintEvent* event) override;
        void mousePressEvent(QMouseEvent* event) override;
        void mouseReleaseEvent(QMouseEvent* event) override;
        void mouseMoveEvent(QMouseEvent* event) override;

    private:
        void setupActions();
        void drawControlPoints(QPainter* p);
        void drawTangentLines(QPainter& p);

        virtual QSize sizeHint() const override {
            return QSize(300, 100);
        }

    private slots:
        void clearAction();
        void showControlPointsChecked(bool checked);
        void showTangentLinesChecked(bool checked);
        void tangentFactorValueChanged(int val);
        void catmullRomFactorValueChanged(double val);
        void cardinalToggled(bool toggled);
        void catmullRomToggled(bool toggled);
    };

} /* namespace rev */

#endif /* CARDINALSPLINEWIDGET_H_ */
