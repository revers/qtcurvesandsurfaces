/*
 * CardinalSplineWidget.cpp
 *
 *  Created on: 20-02-2013
 *      Author: Revers
 */

#include "CardinalSplineWidget.h"

#include <iostream>
#include <QApplication>
#include <QPainter>
#include <QButtonGroup>
#include <QMouseEvent>
#include <QRect>

#include "ui_QtCurvesAndSurfacesWindow.h"
#include "../rev/RevCubicCurve.h"

using namespace rev;
using namespace std;

typedef TCubicCurve2D<scalar_t> CubicCurve2D;
typedef TCubicBaseMatrix<scalar_t> CubicBaseMatrix;

CardinalSplineWidget::CardinalSplineWidget(Ui::MainWindow* mainWindow,
        QWidget* parent) :
        bgColor(203, 214, 230) {
    this->mainWindow = mainWindow;
    mainWindow->cardSplineTangentFactorSB->setValue(tangentFactor);
    mainWindow->cardSplineTangentFactorSB->setSingleStep(50);

    QButtonGroup* group = new QButtonGroup(this);
    setupActions();
}

CardinalSplineWidget::~CardinalSplineWidget() {
}

void CardinalSplineWidget::setupActions() {
    connect(mainWindow->cardSplineClearB, SIGNAL(clicked()),
            this, SLOT(clearAction()));

    connect(mainWindow->cardSplineShowLinesCB, SIGNAL(toggled(bool)),
            this, SLOT(showTangentLinesChecked(bool)));

    connect(mainWindow->cardSplineShowPointsCB, SIGNAL(toggled(bool)),
            this, SLOT(showControlPointsChecked(bool)));

    connect(mainWindow->cardSplineTangentFactorSB, SIGNAL(valueChanged(int)),
            this, SLOT(tangentFactorValueChanged(int)));

    connect(mainWindow->cardSplineCRFactorSB, SIGNAL(valueChanged(double)),
            this, SLOT(catmullRomFactorValueChanged(double)));

    connect(mainWindow->cardSplineCatmullRomRB, SIGNAL(toggled(bool)),
            this, SLOT(catmullRomToggled(bool)));

    connect(mainWindow->cardSplineCardRB, SIGNAL(toggled(bool)),
            this, SLOT(cardinalToggled(bool)));
}

void CardinalSplineWidget::clear() {
    points.clear();
    focusPointIndex = -1;
    mainWindow->cardSplinePointsL->setText(QString("Points: 0"));
    repaint();
}

void CardinalSplineWidget::mousePressEvent(QMouseEvent* event) {
    if (event->button() == Qt::LeftButton) {
        QPoint p = event->pos();
        mousePressed = true;

        if (!hitPoint(p)) {
            focusPointIndex = points.size();
            points.push_back(vec2_t(p.x(), p.y()));
            mainWindow->cardSplinePointsL->setText(
                    QString("Points: %1").arg(points.size()));
        }
    }

    repaint();
}

void CardinalSplineWidget::mouseReleaseEvent(QMouseEvent* event) {
    mousePressed = false;
    focusPointIndex = -1;
    repaint();
}

void CardinalSplineWidget::mouseMoveEvent(QMouseEvent* event) {
    if (!mousePressed) {
        return;
    }

    moveFocusedPoint(event->pos());
    repaint();
}

bool CardinalSplineWidget::hitPoint(const QPoint& p) {
    int index = 0;
    for (vec2_t& v : points) {
        if (glm::distance(vec2_t(p.x(), p.y()), v) <= POINT_RADIUS + 2) {
            focusPointIndex = index;
            return true;
        }
        index++;
    }
    focusPointIndex = -1;
    return false;
}

void CardinalSplineWidget::moveFocusedPoint(const QPoint& p) {
    if (focusPointIndex < 0) {
        return;
    }

    points[focusPointIndex] = vec2_t(p.x(), p.y());
}

void CardinalSplineWidget::drawTangentLines(QPainter& p) {
}

void CardinalSplineWidget::paintEvent(QPaintEvent* event) {
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    painter.setPen(bgColor);
    painter.setBrush(QBrush(bgColor));
    painter.drawRect(0, 0, width() - 1, height() - 1);

    for (int i = 0; points.size() >= 4 && i <= points.size() - 4; i++) {

        const vec2_t& p1 = points[i];
        const vec2_t& p2 = points[i + 1];
        const vec2_t& p3 = points[i + 2];
        const vec2_t& p4 = points[i + 3];

        if (useCatmullRom) {
            scalar_t s(catmullRomFactor);
            scalar_t catmull_rom[4][4] {
                    { -s, 2.0 - s, s - 2.0, s },
                    { 2.0 * s, s - 3.0, 3.0 - 2.0 * s, -s },
                    { -s, 0.0, s, 0.0 },
                    { 0.0, 1.0, 0.0, 0.0 }
            };

            CubicCurve2D curve(catmull_rom, p1, p2, p3, p4);
            painter.setPen(Qt::red);

            for (scalar_t t = 0; t < 1; t += 0.01) {
                vec2_t v1 = curve.eval(t);
                vec2_t v2 = curve.eval(t + 0.01);

                painter.drawLine((int) v1.x, (int) v1.y,
                        (int) v2.x, (int) v2.y);
            }
        } else {
            scalar_t factor(tangentFactor);
            vec2_t d1 = glm::normalize(p3 - p1) * factor;
            vec2_t d2 = glm::normalize(p4 - p2) * factor;

            CubicCurve2D curve(CubicBaseMatrix::HERMITE,
                    p2, p3, d1, d2);
            painter.setPen(Qt::red);

            for (scalar_t t = 0; t < 1; t += 0.01) {
                vec2_t v1 = curve.eval(t);
                vec2_t v2 = curve.eval(t + 0.01);

                painter.drawLine((int) v1.x, (int) v1.y,
                        (int) v2.x, (int) v2.y);
            }
        }

        if (showTangentLines) {
            painter.setPen(QColor(180, 180, 180));

            painter.drawLine(p1.x, p1.y, p3.x, p3.y);
            painter.drawLine(p2.x, p2.y, p4.x, p4.y);
        }
    }

    if (showControlPoints) {
        drawControlPoints(&painter);
    }
}

void CardinalSplineWidget::drawControlPoints(QPainter* p) {
    QPainter& painter = *p;

    for (int i = 0; i < points.size(); i++) {
        if (i == focusPointIndex) {
            painter.setBrush(QBrush(Qt::red));
            painter.setPen(Qt::red);
        } else if (i < 1) {
            painter.setBrush(QBrush(Qt::black));
            painter.setPen(Qt::black);
        } else if (i == points.size() - 1) {
            painter.setBrush(QBrush(Qt::blue));
            painter.setPen(Qt::blue);
        } else {
            painter.setBrush(QBrush(Qt::green));
            painter.setPen(Qt::green);
        }

        const vec2_t& v = points[i];
        painter.drawEllipse((int) v.x - POINT_RADIUS,
                (int) v.y - POINT_RADIUS, POINT_RADIUS * 2 + 1,
                POINT_RADIUS * 2);
    }
}

void CardinalSplineWidget::clearAction() {
    clear();
}

void CardinalSplineWidget::showControlPointsChecked(bool checked) {
    showControlPoints = checked;
    repaint();
}

void CardinalSplineWidget::showTangentLinesChecked(bool checked) {
    showTangentLines = checked;
    repaint();
}

void CardinalSplineWidget::tangentFactorValueChanged(int val) {
    tangentFactor = val;
    repaint();
}

void CardinalSplineWidget::catmullRomFactorValueChanged(double val) {
    catmullRomFactor = val;
    repaint();
}

void CardinalSplineWidget::cardinalToggled(bool toggled) {
    useCatmullRom = !toggled;
    repaint();
}

void CardinalSplineWidget::catmullRomToggled(bool toggled) {
    useCatmullRom = toggled;
    repaint();
}
