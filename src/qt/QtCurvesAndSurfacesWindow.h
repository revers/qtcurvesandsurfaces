/* 
 * File:   TestQtAppForm.h
 * Author: Kamil
 *
 * Created on 13 czerwiec 2012, 13:32
 */

#ifndef _QTCURVESANDSURFACESWINDOW_H_
#define	_QTCURVESANDSURFACESWINDOW_H_

#include "ui_QtCurvesAndSurfacesWindow.h"

#include "Cubic2DWidget.h"

namespace rev {
    class LagrangePolynomialWidget;
    class HermiteInterpolationWidget;
    class CardinalSplineWidget;
    class BezierCurveWidget;
    class BSplineWidget;
}

class QEvent;
class QMouseEvent;
class QImage;
class QCloseEvent;

class QtCurvesAndSurfacesWindow: public QMainWindow {
Q_OBJECT

private:
    Ui::MainWindow widget;
    rev::Cubic2DWidget* cubic2DWidget = nullptr;
    rev::LagrangePolynomialWidget* lagrangePolyWidget = nullptr;
    rev::HermiteInterpolationWidget* hermiteInterpWidget = nullptr;
    rev::CardinalSplineWidget* cardinalSplineWidget = nullptr;
    rev::BezierCurveWidget* bezierCurveWidget = nullptr;
    rev::BSplineWidget* bsplineWidget = nullptr;

public:

    QtCurvesAndSurfacesWindow();
    virtual ~QtCurvesAndSurfacesWindow();

private:
    void setupActions();

private slots:
    void aboutAction();
};

#endif	/* _QTCURVESANDSURFACESWINDOW_H_ */
