/*
 * File:   TestQtAppMain.cpp
 * Author: Kamil
 *
 * Created on 13 czerwiec 2012, 13:32
 */

#include <iostream>
#include <GL/glew.h>

#include <QDesktopWidget>
#include <QApplication>
#include <QGLFormat>

#include <glm/glm.hpp>

#include "qt/QtCurvesAndSurfacesWindow.h"
#include <rev/common/RevErrorStream.h>

using namespace std;

void center(QWidget &widget) {

    int width = widget.width();
    int height = widget.height();

    QDesktopWidget* desktop = QApplication::desktop();

    int screenWidth = desktop->width();
    int screenHeight = desktop->height();

    int x = (screenWidth - width) / 2;
    int y = 25; //(screenHeight - height) / 2;

    widget.setGeometry(x, y, width, height);
}

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);
    app.setApplicationName("QtCurvesAndSurfaces");
    app.setOrganizationName("revers");

    QtCurvesAndSurfacesWindow window;
    
    window.show();
    center(window);

    int qtStatus = app.exec();

    return qtStatus;
}
