/*
 * RevTCubic2D.h
 *
 *  Created on: 22-11-2012
 *      Author: Revers
 */

#ifndef REVCUBIC2D_H_
#define REVCUBIC2D_H_


/** index for COLUMN ordered matrix */
#define IDX(i, j) (j * 4 + i)

#include "RevVecAndMatTypes.hpp"

namespace rev {

    template<typename T>
    class TCubic2D {
    public:
        typedef T scalar_type;
        typedef glm::detail::tvec2<scalar_type> vec2_type;
        typedef glm::detail::tmat4x4<scalar_type> mat_type;

    private:
        vec2_type a, b, c, d;

    public:

        /**
         * @param M is COLUMN ordered matrix.
         */
        TCubic2D(const mat_type& M,
                const vec2_type& G1, const vec2_type& G2,
                const vec2_type& G3, const vec2_type& G4) {
            a = M[0][0] * G1 + M[1][0] * G2 + M[2][0] * G3 + M[3][0] * G4;
            b = M[0][1] * G1 + M[1][1] * G2 + M[2][1] * G3 + M[3][1] * G4;
            c = M[0][2] * G1 + M[1][2] * G2 + M[2][2] * G3 + M[3][2] * G4;
            d = M[0][3] * G1 + M[1][3] * G2 + M[2][3] * G3 + M[3][3] * G4;
        }

        /**
         * @param M is COLUMN ordered matrix.
         */
        TCubic2D(scalar_type* M,
                const vec2_type& G1, const vec2_type& G2,
                const vec2_type& G3, const vec2_type& G4) {

            a = M[IDX(0, 0)] * G1 + M[IDX(0, 1)] * G2 + M[IDX(0, 2)] * G3
            + M[IDX(0, 3)] * G4;
            b = M[IDX(1, 0)] * G1 + M[IDX(1, 1)] * G2 + M[IDX(1, 2)] * G3
            + M[IDX(1, 3)] * G4;
            c = M[IDX(2, 0)] * G1 + M[IDX(2, 1)] * G2 + M[IDX(2, 2)] * G3
            + M[IDX(2, 3)] * G4;
            d = M[IDX(3, 0)] * G1 + M[IDX(3, 1)] * G2 + M[IDX(3, 2)] * G3
            + M[IDX(3, 3)] * G4;
        }

        vec2_type eval(scalar_type t) {
            return t * (t * (t * a + b) + c) + d;
        }

        vec2_type getFirstDerivative(scalar_type t) {
            return vec2_type(3 * t * t * a.x + 2 * t * b.x + c.x,
                    3 * t * t * a.y + 2 * t * b.y + c.y);
        }

        vec2_type getSecondDerivative(scalar_type t) {
            return vec2_type(6 * t * a.x + 2 * b.x,
                    6 * t * a.y + 2 * b.y);
        }

        ~TCubic2D() {
        }
    };

    typedef TCubic2D<float> Cubic2Df;
    typedef TCubic2D<double> Cubic2Dd;

} /* namespace rev */

#endif /* REVCUBIC2D_H_ */
