/*
 * RevBaseMatrix.cpp
 *
 *  Created on: 23-11-2012
 *      Author: Revers
 */

#include "RevBaseMatrix.h"

// =================================================================
// Float Matrices:
// =================================================================

/**
 * HERMITE
 *      |  2  -2   1   1 |
 * h =  | -3   3  -2  -1 |
 *      |  0   0   1   0 |
 *      |  1   0   0   0 |
 */
template<>
const glm::detail::tmat4x4<double> rev::BaseMatrix<double>::HERMITE(
        2.0, -3.0, 0.0, 1.0,   // first COLUMN
        -2.0, 3.0, 0.0, 0.0,   // second COLUMN
        1.0, -2.0, 1.0, 0.0,   // third COLUMN
        1.0, -1.0, 0.0, 0.0);  // fourth COLUMN

/**
 * BEZIER:
 *      | -1   3  -3   1 |
 * b =  |  3  -6   3   0 |
 *      | -3   3   0   0 |
 *      |  1   0   0   0 |
 */
template<>
const glm::detail::tmat4x4<double> rev::BaseMatrix<double>::BEZIER(
        -1.0, 3.0, -3.0, 1.0,  // first COLUMN
        3.0, -6.0, 3.0, 0.0,  // second COLUMN
        -3.0, 3.0, 0.0, 0.0,  // third COLUMN
        1.0, 0.0, 0.0, 0.0); // fourth COLUMN

/**
 * BSPLINE
 *      | -1/6   3/6  -3/6   1/6 |
 * bs = |  3/6  -6/6   3/6   0   |
 *      | -3/6   0     3/6   0   |
 *      |  1/6   4/6   1/6   0   |
 */

template<>
const glm::detail::tmat4x4<double> rev::BaseMatrix<double>::BSPLINE(
        -1.0 / 6.0, 3.0 / 6.0, -3.0 / 6.0, 1.0 / 6.0, // first COLUMN
        3.0 / 6.0, -6.0 / 6.0, 0.0, 4.0 / 6.0, // second COLUMN
        -3.0 / 6.0, 3.0 / 6.0, 3.0 / 6.0, 1.0 / 6.0, // third COLUMN
        1.0 / 6.0, 0.0, 0.0, 0.0);    // fourth COLUMN

/**
 * CATMULL_ROM
 *      | -0.5   1.5  -1.5   0.5 |
 * c =  |  1    -2.5   2    -0.5 |
 *      | -0.5   0     0.5   0   |
 *      |  0     1     0     0   |
 */
template<>
const glm::detail::tmat4x4<double> rev::BaseMatrix<double>::CATMULL_ROM(
        -0.5, 1.0, -0.5, 0.0,  // first COLUMN
        1.5, -2.5, 0.0, 1.0,  // second COLUMN
        -1.5, 2.0, 0.5, 0.0,  // third COLUMN
        0.5, -0.5, 0.0, 0.0); // fourth COLUMN

// =================================================================
// Double Matrices:
// =================================================================

/**
 * HERMITE
 *      |  2  -2   1   1 |
 * h =  | -3   3  -2  -1 |
 *      |  0   0   1   0 |
 *      |  1   0   0   0 |
 */
template<>
const glm::detail::tmat4x4<float> rev::BaseMatrix<float>::HERMITE(
        2.0, -3.0, 0.0, 1.0,   // first COLUMN
        -2.0, 3.0, 0.0, 0.0,   // second COLUMN
        1.0, -2.0, 1.0, 0.0,   // third COLUMN
        1.0, -1.0, 0.0, 0.0);  // fourth COLUMN

/**
 * BEZIER:
 *      | -1   3  -3   1 |
 * b =  |  3  -6   3   0 |
 *      | -3   3   0   0 |
 *      |  1   0   0   0 |
 */
template<>
const glm::detail::tmat4x4<float> rev::BaseMatrix<float>::BEZIER(
        -1.0, 3.0, -3.0, 1.0,  // first COLUMN
        3.0, -6.0, 3.0, 0.0,  // second COLUMN
        -3.0, 3.0, 0.0, 0.0,  // third COLUMN
        1.0, 0.0, 0.0, 0.0); // fourth COLUMN

/**
 * BSPLINE
 *      | -1/6   3/6  -3/6   1/6 |
 * bs = |  3/6  -6/6   3/6   0   |
 *      | -3/6   0     3/6   0   |
 *      |  1/6   4/6   1/6   0   |
 */
template<>
const glm::detail::tmat4x4<float> rev::BaseMatrix<float>::BSPLINE(
        -1.0 / 6.0, 3.0 / 6.0, -3.0 / 6.0, 1.0 / 6.0, // first COLUMN
        3.0 / 6.0, -6.0 / 6.0, 0.0, 4.0 / 6.0, // second COLUMN
        -3.0 / 6.0, 3.0 / 6.0, 3.0 / 6.0, 1.0 / 6.0, // third COLUMN
        1.0 / 6.0, 0.0, 0.0, 0.0);    // fourth COLUMN

/**
 * CATMULL_ROM
 *      | -0.5   1.5  -1.5   0.5 |
 * c =  |  1    -2.5   2    -0.5 |
 *      | -0.5   0     0.5   0   |
 *      |  0     1     0     0   |
 */
template<>
const glm::detail::tmat4x4<float> rev::BaseMatrix<float>::CATMULL_ROM(
        -0.5, 1.0, -0.5, 0.0,  // first COLUMN
        1.5, -2.5, 0.0, 1.0,  // second COLUMN
        -1.5, 2.0, 0.5, 0.0,  // third COLUMN
        0.5, -0.5, 0.0, 0.0); // fourth COLUMN
