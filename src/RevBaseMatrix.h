/*
 * RevBaseMatrix.h
 *
 *  Created on: 23-11-2012
 *      Author: Revers
 */

#ifndef REVBASEMATRIX_H_
#define REVBASEMATRIX_H_

#include <glm/glm.hpp>

namespace rev {

    template<typename T>
    class BaseMatrix {
        BaseMatrix();

    public:

        static const glm::detail::tmat4x4<T> HERMITE;
        static const glm::detail::tmat4x4<T> BEZIER;
        static const glm::detail::tmat4x4<T> BSPLINE;
        static const glm::detail::tmat4x4<T> CATMULL_ROM;

    };
}

template<>
const glm::detail::tmat4x4<float> rev::BaseMatrix<float>::HERMITE;
template<>
const glm::detail::tmat4x4<double> rev::BaseMatrix<double>::HERMITE;

template<>
const glm::detail::tmat4x4<float> rev::BaseMatrix<float>::BEZIER;
template<>
const glm::detail::tmat4x4<double> rev::BaseMatrix<double>::BEZIER;

template<>
const glm::detail::tmat4x4<float> rev::BaseMatrix<float>::BSPLINE;
template<>
const glm::detail::tmat4x4<double> rev::BaseMatrix<double>::BSPLINE;

template<>
const glm::detail::tmat4x4<float> rev::BaseMatrix<float>::CATMULL_ROM;
template<>
const glm::detail::tmat4x4<double> rev::BaseMatrix<double>::CATMULL_ROM;
/* namespace rev */
#endif /* REVBASEMATRIX_H_ */
