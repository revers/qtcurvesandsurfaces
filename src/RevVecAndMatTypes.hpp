/*
 * RevVecAndMatTypes.h
 *
 *  Created on: 24-11-2012
 *      Author: Revers
 */

#ifndef REVVECANDMATTYPES_H_
#define REVVECANDMATTYPES_H_

#include <glm/glm.hpp>

namespace rev {

    typedef glm::detail::tvec2<float> vec2f;
    typedef glm::detail::tvec2<double> vec2d;

    typedef glm::detail::tvec3<float> vec3f;
    typedef glm::detail::tvec3<double> vec3d;

    typedef glm::detail::tvec4<float> vec4f;
    typedef glm::detail::tvec4<double> vec4d;

    typedef glm::detail::tmat2x2<float> mat2f;
    typedef glm::detail::tmat2x2<double> mat2d;

    typedef glm::detail::tmat3x3<float> mat3f;
    typedef glm::detail::tmat3x3<double> mat3d;

    typedef glm::detail::tmat4x4<float> mat4f;
    typedef glm::detail::tmat4x4<double> mat4d;

} /* namespace rev */
#endif /* REVVECANDMATTYPES_H_ */
