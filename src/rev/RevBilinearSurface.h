/*
 * RevBilinearSurface.h
 *
 *  Created on: 07-12-2012
 *      Author: Revers
 */

#ifndef REVBILINEARSURFACE_H_
#define REVBILINEARSURFACE_H_

#include <glm/glm.hpp>

namespace rev {

    //----------------------------------------------------------------
    // TBilinearSurface
    //================================================================
    template<typename T1>
    class TBilinearSurface {
    public:
        typedef T1 scalar_t;
        typedef glm::detail::tvec3<scalar_t> vec3_t;

        /**
         *     | -1  1 |
         * M = |       |
         *     |  1  0 |
         */
        static const scalar_t BEZIER_BASIS[2][2];

    private:
        vec3_t C[2][2];

    public:
        TBilinearSurface(
                const vec3_t& P00,
                const vec3_t& P01,
                const vec3_t& P10,
                const vec3_t& P11) {
            init(BEZIER_BASIS, P00, P01, P10, P11);
        }

        /**
         * @param M is ROW ordered matrix.
         */
        TBilinearSurface(const scalar_t M[2][2],
                const vec3_t& P00,
                const vec3_t& P01,
                const vec3_t& P10,
                const vec3_t& P11) {
            init(M, P00, P01, P10, P11);
        }

        /**
         * @param P is ROW ordered matrix.
         */
        TBilinearSurface(vec3_t P[2][2]) {
            init(BEZIER_BASIS, P);
        }

        /**
         * @param M & P are ROW ordered matrices.
         */
        TBilinearSurface(const scalar_t M[2][2],
                vec3_t P[2][2]) {
            init(M, P);
        }

        vec3_t eval(scalar_t u, scalar_t w) {
            // U * (C * W^T)
            return u * (w * C[0][0] + C[0][1])
                    + (w * C[1][0] + C[1][1]);
        }

        vec3_t normal(scalar_t u, scalar_t w) {
            vec3_t du = C[0][0] * w + C[0][1];
            vec3_t dw = C[0][0] * u + C[1][0];
            return glm::normalize(glm::cross(du, dw));
        }

    private:
        /**
         * @param M is ROW ordered matrix.
         */
        void init(const scalar_t M[2][2],
                const vec3_t& P00,
                const vec3_t& P01,
                const vec3_t& P10,
                const vec3_t& P11);

        /**
         * @param M & P are ROW ordered matrices.
         */
        void init(const scalar_t M[2][2], vec3_t P[2][2]);
    };

    //----------------------------------------------------------------
    // General implementation
    //================================================================

    template<typename T1>
    void TBilinearSurface<T1>::init(const scalar_t M[2][2],
            vec3_t P[2][2]) {
        // U(u) = (u, 1)
        // W(w) = (w, 1)
        // P(u, w) = U(u) * M * P * M^T * W(w)^T
        // P(u, w) = U(u) * M * T * W(w)^T
        // P(u, w) = U(u) * C * W(w)^T
        vec3_t T[2][2];

        // T = P  * M^T
        const int dim = 2;
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                T[i][j] = vec3_t(0);
                for (int k = 0; k < dim; k++) {
                    T[i][j] += P[i][k] * M[j][k];
                }
            }
        }

        // C = M * T
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                C[i][j] = vec3_t(0);
                for (int k = 0; k < dim; k++) {
                    C[i][j] += M[i][k] * T[k][j];
                }
            }
        }
    }

    template<typename T1>
    void TBilinearSurface<T1>::init(const scalar_t M[2][2],
            const vec3_t& P00,
            const vec3_t& P01,
            const vec3_t& P10,
            const vec3_t& P11) {
//        vec3_t P[2][2] {
//                { P00, P01 },
//                { P10, P11 },
//        };

        vec3_t P[2][2] {
                { P00, P10 },
                { P01, P11 },
        };

        TBilinearSurface<T1>::init(M, P);
    }

    //----------------------------------------------------------------
    // Double specializations:
    //================================================================

    template<>
    const TBilinearSurface<double>::scalar_t
    TBilinearSurface<double>::BEZIER_BASIS[2][2];

    //----------------------------------------------------------------
    // Float specializations:
    //================================================================

    template<>
    const TBilinearSurface<float>::scalar_t
    TBilinearSurface<float>::BEZIER_BASIS[2][2];

    //----------------------------------------------------------------
    // Type Definitions:
    //================================================================
    typedef TBilinearSurface<float> BilinearSurfacef;
    typedef TBilinearSurface<double> BilinearSurfaced;
} // end namespace rev;

#endif /* REVBILINEARSURFACE_H_ */
