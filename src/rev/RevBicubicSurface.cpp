/*
 * RevBicubicSurface.cpp
 *
 *  Created on: 09-12-2012
 *      Author: Revers
 */

#include "RevBicubicSurface.h"

using namespace rev;

//----------------------------------------------------------------
// Double specializations:
//================================================================

/**
 *     | -1   3  -3   1 |
 * M = |  3  -6   3   0 |
 *     | -3   3   0   0 |
 *     |  1   0   0   0 |
 */
template<>
const TBicubicSurface<double>::scalar_t
TBicubicSurface<double>::BEZIER_BASIS[4][4] {
        { -1.0, 3.0, -3.0, 1.0 },
        { 3.0, -6.0, 3.0, 0.0 },
        { -3.0, 3.0, 0.0, 0.0 },
        { 1.0, 0.0, 0.0, 0.0 }
};

/**
 *     | -4.5  13.5 -13.5  4.5 |
 * M = |  9.0 -22.5  18.0 -4.5 |
 *     | -5.5   9.0  -4.5  1.0 |
 *     |  1.0   0.0   0.0  0.0 |
 */
template<>
const TBicubicSurface<double>::scalar_t
TBicubicSurface<double>::EQUIDISTANT_BASIS[4][4] {
    { -4.5,  13.5, -13.5,  4.5 },
    {  9.0, -22.5,  18.0, -4.5 },
    { -5.5,   9.0,  -4.5,  1.0 },
    {  1.0,   0.0,   0.0,  0.0 }
};

//----------------------------------------------------------------
// Float specializations:
//================================================================

/**
 *     | -1   3  -3   1 |
 * M = |  3  -6   3   0 |
 *     | -3   3   0   0 |
 *     |  1   0   0   0 |
 */
template<>
const TBicubicSurface<float>::scalar_t
TBicubicSurface<float>::BEZIER_BASIS[4][4] {
        { -1.0f, 3.0f, -3.0f, 1.0f },
        { 3.0f, -6.0f, 3.0f, 0.0f },
        { -3.0f, 3.0f, 0.0f, 0.0f },
        { 1.0f, 0.0f, 0.0f, 0.0f }
};

/**
 *     | -4.5  13.5 -13.5  4.5 |
 * M = |  9.0 -22.5  18.0 -4.5 |
 *     | -5.5   9.0  -4.5  1.0 |
 *     |  1.0   0.0   0.0  0.0 |
 */
template<>
const TBicubicSurface<float>::scalar_t
TBicubicSurface<float>::EQUIDISTANT_BASIS[4][4] {
    { -4.5f,  13.5f, -13.5f,  4.5f },
    {  9.0f, -22.5f,  18.0f, -4.5f },
    { -5.5f,   9.0f,  -4.5f,  1.0f },
    {  1.0f,   0.0f,   0.0f,  0.0f }
};
