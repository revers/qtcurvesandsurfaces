/*
 * RevBilinearSurface.cpp
 *
 *  Created on: 08-12-2012
 *      Author: Revers
 */
#include "RevBilinearSurface.h"

using namespace rev;

//----------------------------------------------------------------
// Double specializations:
//================================================================

/**
 *     | -1  1 |
 * M = |       |
 *     |  1  0 |
 */
template<>
const TBilinearSurface<double>::scalar_t
TBilinearSurface<double>::BEZIER_BASIS[2][2] {
    {-1.0, 1.0 },
    {1.0, 0.0 }
};

//----------------------------------------------------------------
// Float specializations:
//================================================================

/**
 *     | -1  1 |
 * M = |       |
 *     |  1  0 |
 */
template<>
const TBilinearSurface<float>::scalar_t
TBilinearSurface<float>::BEZIER_BASIS[2][2] {
    {-1.0f, 1.0f },
    {1.0f, 0.0f }
};
