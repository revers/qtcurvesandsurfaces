/*
 * RevBiquadraticSurface.cpp
 *
 *  Created on: 08-12-2012
 *      Author: Revers
 */

#include "RevBiquadraticSurface.h"

using namespace rev;

//----------------------------------------------------------------
// Double specializations:
//================================================================

/**
 *     |  1  -2   1 |
 * M = | -2   2   0 |
 *     |  1   0   0 |
 */
template<>
const TBiquadraticSurface<double>::scalar_t
TBiquadraticSurface<double>::BEZIER_BASIS[3][3] {
    {1.0, -2.0, 1.0 },
    {-2.0, 2.0, 0.0 },
    {1.0, 0.0, 0.0 }
};
/**
 *     |  2  -4   2 |
 * M = | -3   4  -1 |
 *     |  1   0   0 |
 */
template<>
const TBiquadraticSurface<double>::scalar_t
TBiquadraticSurface<double>::LAGRANGE_BASIS[3][3] {
    {2.0, -4.0, 2.0 },
    {-3.0, 4.0, -1.0 },
    {1.0, 0.0, 0.0}
};

//----------------------------------------------------------------
// Float specializations:
//================================================================

/**
 *     |  1  -2   1 |
 * M = | -2   2   0 |
 *     |  1   0   0 |
 */
template<>
const TBiquadraticSurface<float>::scalar_t
TBiquadraticSurface<float>::BEZIER_BASIS[3][3] {
    {1.0f, -2.0f, 1.0f },
    {-2.0f, 2.0f, 0.0f },
    {1.0f, 0.0f, 0.0f }
};

/**
 *     |  2  -4   2 |
 * M = | -3   4  -1 |
 *     |  1   0   0 |
 */
template<>
const TBiquadraticSurface<float>::scalar_t
TBiquadraticSurface<float>::LAGRANGE_BASIS[3][3] {
    {2.0f, -4.0f, 2.0f },
    {-3.0f, 4.0f, -1.0f },
    {1.0f, 0.0f, 0.0f}
};
