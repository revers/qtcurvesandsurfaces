/*
 * RevBiquadraticSurface.h
 *
 *  Created on: 08-12-2012
 *      Author: Revers
 */

#ifndef REVBIQUADRATICSURFACE_H_
#define REVBIQUADRATICSURFACE_H_

#include <glm/glm.hpp>

namespace rev {

    //----------------------------------------------------------------
    // TBiquadraticSurface
    //================================================================
    template<typename T1>
    class TBiquadraticSurface {
    public:
        typedef T1 scalar_t;
        typedef glm::detail::tvec3<scalar_t> vec3_t;
        //typedef glm::detail::tmat3x3<scalar_t> scalar_t;

        /**
         *     |  1  -2   1 |
         * M = | -2   2   0 |
         *     |  1   0   0 |
         */
        static const scalar_t BEZIER_BASIS[3][3];

        /**
         *     |  2  -4   2 |
         * M = | -3   4  -1 |
         *     |  1   0   0 |
         */
        static const scalar_t LAGRANGE_BASIS[3][3];

    private:
        vec3_t C[3][3];

    public:
        TBiquadraticSurface(
                const vec3_t& P00,
                const vec3_t& P01,
                const vec3_t& P02,
                const vec3_t& P10,
                const vec3_t& P11,
                const vec3_t& P12,
                const vec3_t& P20,
                const vec3_t& P21,
                const vec3_t& P22) {
            init(BEZIER_BASIS, P00, P01, P02, P10,
                    P11, P12, P20, P21, P22);
        }

        /**
         * @param M is ROW ordered matrix.
         */
        TBiquadraticSurface(const scalar_t M[3][3],
                const vec3_t& P00,
                const vec3_t& P01,
                const vec3_t& P02,
                const vec3_t& P10,
                const vec3_t& P11,
                const vec3_t& P12,
                const vec3_t& P20,
                const vec3_t& P21,
                const vec3_t& P22) {
            init(M, P00, P01, P02, P10,
                    P11, P12, P20, P21, P22);
        }

        /**
         * @param P is ROW ordered matrix.
         */
        TBiquadraticSurface(vec3_t P[3][3]) {
            init(BEZIER_BASIS, P);
        }

        /**
         * M & P are ROW ordered matrices.
         */
        TBiquadraticSurface(const scalar_t M[3][3],
                vec3_t P[3][3]) {
            init(M, P);
        }

        vec3_t eval(scalar_t u, scalar_t w) {
            // U * (C * W^T)
            return u * (u
                    * (w * (w * C[0][0] + C[0][1]) + C[0][2])
                    + (w * (w * C[1][0] + C[1][1]) + C[1][2]))
                    + (w * (w * C[2][0] + C[2][1]) + C[2][2]);
        }

        vec3_t normal(scalar_t u, scalar_t w) {
            vec3_t du = u * 2.0
                    * (w * (w * C[0][0] + C[0][1]) + C[0][2])
                    + (w * (w * C[1][0] + C[1][1]) + C[1][2]);

            vec3_t dw = w * 2.0
                    * (u * (u * C[0][0] + C[1][0]) + C[2][0])
                    + (u * (u * C[0][1] + C[1][1]) + C[2][1]);

            return glm::normalize(glm::cross(du, dw));
        }

    private:
        /**
         * @param M is ROW ordered matrix.
         */
        void init(const scalar_t M[3][3],
                const vec3_t& P00,
                const vec3_t& P01,
                const vec3_t& P02,
                const vec3_t& P10,
                const vec3_t& P11,
                const vec3_t& P12,
                const vec3_t& P20,
                const vec3_t& P21,
                const vec3_t& P22);

        /**
         * M & P are ROW ordered matrices.
         */
        void init(const scalar_t M[3][3], vec3_t P[3][3]);
    };

    //----------------------------------------------------------------
    // General implementation
    //================================================================

    template<typename T1>
    void TBiquadraticSurface<T1>::init(const scalar_t M[3][3],
            vec3_t P[3][3]) {
        // U(u) = (u^2, u, 1)
        // W(w) = (w^2, w, 1)
        // P(u, w) = U(u) * M * P * M^T * W(w)^T
        // P(u, w) = U(u) * M * T * W(w)^T
        // P(u, w) = U(u) * C * W(w)^T
        vec3_t T[3][3];

        // T = P  * M^T
        int dim = 3;
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                T[i][j] = vec3_t(0);
                for (int k = 0; k < dim; k++) {
                    T[i][j] += P[i][k] * M[j][k];
                }
            }
        }

        // C = M * T
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                C[i][j] = vec3_t(0);
                for (int k = 0; k < dim; k++) {
                    C[i][j] += M[i][k] * T[k][j];
                }
            }
        }
    }

    template<typename T1>
    void TBiquadraticSurface<T1>::init(const scalar_t M[3][3],
            const vec3_t& P00,
            const vec3_t& P01,
            const vec3_t& P02,
            const vec3_t& P10,
            const vec3_t& P11,
            const vec3_t& P12,
            const vec3_t& P20,
            const vec3_t& P21,
            const vec3_t& P22) {

//        vec3_t P[3][3] {
//                { P00, P01, P02 },
//                { P10, P11, P12 },
//                { P20, P21, P22 }
//        };

        vec3_t P[3][3] {
                { P00, P10, P20 },
                { P01, P11, P21 },
                { P02, P12, P22 }
        };

        TBiquadraticSurface<T1>::init(M, P);
    }

    //----------------------------------------------------------------
    // Double specializations:
    //================================================================

    template<>
    const TBiquadraticSurface<double>::scalar_t
    TBiquadraticSurface<double>::BEZIER_BASIS[3][3];

    template<>
    const TBiquadraticSurface<double>::scalar_t
    TBiquadraticSurface<double>::LAGRANGE_BASIS[3][3];

    //----------------------------------------------------------------
    // Float specializations:
    //================================================================

    template<>
    const TBiquadraticSurface<float>::scalar_t
    TBiquadraticSurface<float>::BEZIER_BASIS[3][3];

    template<>
    const TBiquadraticSurface<float>::scalar_t
    TBiquadraticSurface<float>::LAGRANGE_BASIS[3][3];

    //----------------------------------------------------------------
    // Type Definitions:
    //================================================================
    typedef TBiquadraticSurface<float> BiquadraticSurfacef;
    typedef TBiquadraticSurface<double> BiquadraticSurfaced;
} // end namespace rev;
#endif /* REVBIQUADRATICSURFACE_H_ */
