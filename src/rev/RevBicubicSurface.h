/*
 * RevBicubicSurface.h
 *
 *  Created on: 09-12-2012
 *      Author: Revers
 */

#ifndef REVBICUBICSURFACE_H_
#define REVBICUBICSURFACE_H_

#include <glm/glm.hpp>

namespace rev {

    //----------------------------------------------------------------
    // TBicubicSurface
    //================================================================
    template<typename T1>
    class TBicubicSurface {
    public:
        typedef T1 scalar_t;
        typedef glm::detail::tvec3<scalar_t> vec3_t;

        /**
         *     | -1   3  -3   1 |
         * M = |  3  -6   3   0 |
         *     | -3   3   0   0 |
         *     |  1   0   0   0 |
         */
        static const scalar_t BEZIER_BASIS[4][4];

        /**
         *     | -4.5  13.5 -13.5  4.5 |
         * M = |  9.0 -22.5  18.0 -4.5 |
         *     | -5.5   9.0  -4.5  1.0 |
         *     |  1.0   0.0   0.0  0.0 |
         */
        static const scalar_t EQUIDISTANT_BASIS[4][4];

    private:
        vec3_t C[4][4];

    public:
        TBicubicSurface(
                const vec3_t& P00,
                const vec3_t& P01,
                const vec3_t& P02,
                const vec3_t& P03,
                const vec3_t& P10,
                const vec3_t& P11,
                const vec3_t& P12,
                const vec3_t& P13,
                const vec3_t& P20,
                const vec3_t& P21,
                const vec3_t& P22,
                const vec3_t& P23,
                const vec3_t& P30,
                const vec3_t& P31,
                const vec3_t& P32,
                const vec3_t& P33) {
            init(BEZIER_BASIS[4][4],
                    P00, P01, P02, P03,
                    P10, P11, P12, P13,
                    P20, P21, P22, P23,
                    P30, P31, P32, P33);
        }

        /**
         * @param M is ROW ordered matrix.
         */
        TBicubicSurface(const scalar_t M[4][4],
                const vec3_t& P00,
                const vec3_t& P01,
                const vec3_t& P02,
                const vec3_t& P03,
                const vec3_t& P10,
                const vec3_t& P11,
                const vec3_t& P12,
                const vec3_t& P13,
                const vec3_t& P20,
                const vec3_t& P21,
                const vec3_t& P22,
                const vec3_t& P23,
                const vec3_t& P30,
                const vec3_t& P31,
                const vec3_t& P32,
                const vec3_t& P33) {
            init(M,
                    P00, P01, P02, P03,
                    P10, P11, P12, P13,
                    P20, P21, P22, P23,
                    P30, P31, P32, P33);
        }

        /**
         * @param P is ROW ordered matrix.
         */
        TBicubicSurface(vec3_t P[4][4]) {
            init(BEZIER_BASIS[4][4], P);
        }

        /**
         * M & P are ROW ordered matrices.
         */
        TBicubicSurface(const scalar_t M[4][4],
                vec3_t P[4][4]) {
            init(M, P);
        }

        vec3_t eval(scalar_t u, scalar_t w) {
            // U * (C * W^T)
            return u * (u * (u
                    * (w * (w * (w * C[0][0] + C[0][1]) + C[0][2]) + C[0][3])
                    + (w * (w * (w * C[1][0] + C[1][1]) + C[1][2]) + C[1][3]))
                    + (w * (w * (w * C[2][0] + C[2][1]) + C[2][2]) + C[2][3]))
                    + (w * (w * (w * C[3][0] + C[3][1]) + C[3][2]) + C[3][3]);
        }

        vec3_t normal(scalar_t u, scalar_t w) {
            vec3_t du = u * (u
                    * 3.0 * (w * (w * (w * C[0][0] + C[0][1]) + C[0][2]) + C[0][3])
                    + 2.0 * (w * (w * (w * C[1][0] + C[1][1]) + C[1][2]) + C[1][3]))
                          + (w * (w * (w * C[2][0] + C[2][1]) + C[2][2]) + C[2][3]);

            vec3_t dw = w * (w
                    * 3.0 * (u * (u * (u * C[0][0] + C[1][0]) + C[2][0]) + C[3][0])
                    + 2.0 * (u * (u * (u * C[0][1] + C[1][1]) + C[2][1]) + C[3][1]))
                          + (u * (u * (u * C[0][2] + C[1][2]) + C[2][2]) + C[3][2]);
            return glm::normalize(glm::cross(du, dw));
        }

    private:
        /**
         * @param M is ROW ordered matrix.
         */
        void init(const scalar_t M[4][4],
                const vec3_t& P00,
                const vec3_t& P01,
                const vec3_t& P02,
                const vec3_t& P03,
                const vec3_t& P10,
                const vec3_t& P11,
                const vec3_t& P12,
                const vec3_t& P13,
                const vec3_t& P20,
                const vec3_t& P21,
                const vec3_t& P22,
                const vec3_t& P23,
                const vec3_t& P30,
                const vec3_t& P31,
                const vec3_t& P32,
                const vec3_t& P33);

        /**
         * M & P are ROW ordered matrices.
         */
        void init(const scalar_t M[4][4], vec3_t P[4][4]);
    };

    //----------------------------------------------------------------
    // General implementation
    //================================================================

    template<typename T1>
    void TBicubicSurface<T1>::init(const scalar_t M[4][4],
            vec3_t P[4][4]) {
        // U(u) = (u^3, u^2, u, 1)
        // W(w) = (w^3, w^2, w, 1)
        // P(u, w) = U(u) * M * P * M^T * W(w)^T
        // P(u, w) = U(u) * M * T * W(w)^T
        // P(u, w) = U(u) * C * W(w)^T
        vec3_t T[4][4];

        // T = P  * M^T
        int dim = 4;
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                T[i][j] = vec3_t(0);
                for (int k = 0; k < dim; k++) {
                    T[i][j] += P[i][k] * M[j][k];
                }
            }
        }

        // C = M * T
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                C[i][j] = vec3_t(0);
                for (int k = 0; k < dim; k++) {
                    C[i][j] += M[i][k] * T[k][j];
                }
            }
        }
    }

    template<typename T1>
    void TBicubicSurface<T1>::init(const scalar_t M[4][4],
            const vec3_t& P00,
            const vec3_t& P01,
            const vec3_t& P02,
            const vec3_t& P03,
            const vec3_t& P10,
            const vec3_t& P11,
            const vec3_t& P12,
            const vec3_t& P13,
            const vec3_t& P20,
            const vec3_t& P21,
            const vec3_t& P22,
            const vec3_t& P23,
            const vec3_t& P30,
            const vec3_t& P31,
            const vec3_t& P32,
            const vec3_t& P33) {

        vec3_t P[4][4] {
            { P00, P10, P20, P30 },
            { P01, P11, P21, P31 },
            { P02, P12, P22, P32 },
            { P03, P13, P23, P33 },
        };

        TBicubicSurface<T1>::init(M, P);
    }

    //----------------------------------------------------------------
    // Double specializations:
    //================================================================

    template<>
    const TBicubicSurface<double>::scalar_t
    TBicubicSurface<double>::BEZIER_BASIS[4][4];

    template<>
    const TBicubicSurface<double>::scalar_t
    TBicubicSurface<double>::EQUIDISTANT_BASIS[4][4];

    //----------------------------------------------------------------
    // Float specializations:
    //================================================================

    template<>
    const TBicubicSurface<float>::scalar_t
    TBicubicSurface<float>::BEZIER_BASIS[4][4];

    template<>
    const TBicubicSurface<float>::scalar_t
    TBicubicSurface<float>::EQUIDISTANT_BASIS[4][4];

    //----------------------------------------------------------------
    // Type Definitions:
    //================================================================
    typedef TBicubicSurface<float> BicubicSurfacef;
    typedef TBicubicSurface<double> BicubicSurfaced;
} /* namespace rev */
#endif /* REVBICUBICSURFACE_H_ */
