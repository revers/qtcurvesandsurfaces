/*
 * RevCubicCurve.h
 *
 *  Created on: 18-02-2013
 *      Author: Revers
 */

#ifndef REVCUBICCURVE_H_
#define REVCUBICCURVE_H_

#include <glm/glm.hpp>

namespace rev {

	//----------------------------------------------------------------
	// CubicBaseMatrix
	//================================================================
	template<typename ScalarType>
	struct TCubicBaseMatrix {
		typedef ScalarType scalar_t;

		/**
		 *     | -1   3  -3   1 |
		 * M = |  3  -6   3   0 |
		 *     | -3   3   0   0 |
		 *     |  1   0   0   0 |
		 */
		static const scalar_t BEZIER[4][4];

		/**
		 *     |  2  -2   1   1 |
		 * M = | -3   3  -2  -1 |
		 *     |  0   0   1   0 |
		 *     |  1   0   0   0 |
		 */
		static const scalar_t HERMITE[4][4];

		/**
		 *      | -0.5   1.5  -1.5   0.5 |
		 * M =  |  1    -2.5   2    -0.5 |
		 *      | -0.5   0     0.5   0   |
		 *      |  0     1     0     0   |
		 */
		static const scalar_t CATMULL_ROM[4][4];

		/**
		 *      | -1/6   3/6  -3/6   1/6 |
		 * M  = |  3/6  -6/6   3/6   0   |
		 *      | -3/6   0     3/6   0   |
		 *      |  1/6   4/6   1/6   0   |
		 */
		static const scalar_t BSPLINE[4][4];
	};

	//----------------------------------------------------------------
	// TCubicCurve
	//================================================================
	template<typename ScalarType, template<typename U> class VecType>
	struct TCubicCurve {
		typedef ScalarType scalar_t;
		typedef VecType<scalar_t> vec_t;

	private:
		vec_t A, B, C, D;

	public:
		TCubicCurve(const scalar_t M[4][4],
				const vec_t& G1, const vec_t& G2,
				const vec_t& G3, const vec_t& G4) {
			A = G1 * M[0][0] + G2 * M[0][1] + G3 * M[0][2] + G4 * M[0][3];
			B = G1 * M[1][0] + G2 * M[1][1] + G3 * M[1][2] + G4 * M[1][3];
			C = G1 * M[2][0] + G2 * M[2][1] + G3 * M[2][2] + G4 * M[2][3];
			D = G1 * M[3][0] + G2 * M[3][1] + G3 * M[3][2] + G4 * M[3][3];
		}

		vec_t eval(scalar_t t) {
			return t * (t * (t * A + B) + C) + D;
		}

		/**
		 * First derivative.
		 */
		vec_t dt(scalar_t t) {
			return t * (t * scalar_t(3) * A + scalar_t(2) * B) + C;
		}

		/**
		 * Second derivative.
		 */
		vec_t ddt(scalar_t t) {
			return t * scalar_t(6) * A + scalar_t(2) * B;
		}
	};

	//----------------------------------------------------------------
	// Type Definitions:
	//================================================================
	template<typename T>
	using TCubicCurve2D = TCubicCurve<T, glm::detail::tvec2>;

	template<typename T>
	using TCubicCurve3D = TCubicCurve<T, glm::detail::tvec3>;

	typedef TCubicCurve2D<float> CubicCurve2Df;
	typedef TCubicCurve2D<double> CubicCurve2Dd;

	typedef TCubicCurve3D<float> CubicCurve3Df;
	typedef TCubicCurve3D<double> CubicCurve3Dd;

	typedef TCubicBaseMatrix<float> CubicBaseMatrixf;
	typedef TCubicBaseMatrix<double> CubicBaseMatrixd;

	//----------------------------------------------------------------
	// Double specializations:
	//================================================================
	template<>
	const double TCubicBaseMatrix<double>::BEZIER[4][4];

	template<>
	const double TCubicBaseMatrix<double>::HERMITE[4][4];

	template<>
	const double TCubicBaseMatrix<double>::CATMULL_ROM[4][4];

	template<>
	const double TCubicBaseMatrix<double>::BSPLINE[4][4];

	//----------------------------------------------------------------
	// Float specializations:
	//================================================================
	template<>
	const float TCubicBaseMatrix<float>::BEZIER[4][4];

	template<>
	const float TCubicBaseMatrix<float>::HERMITE[4][4];

	template<>
	const float TCubicBaseMatrix<float>::CATMULL_ROM[4][4];

	template<>
	const float TCubicBaseMatrix<float>::BSPLINE[4][4];

}
/* namespace rev */
#endif /* REVCUBICCURVE_H_ */
